<?php require_once("./configurations.php"); ?>
<?php 
	header("Content-Type: text/xml; charset=iso-8859-1",true);
	header('Content-Disposition: inline; filename=sitemaps.xml');
?>
<?php include("./kernel/php-function/get_date.php"); ?>
<?php include("./kernel/php-function/get_navigation_bar.php"); ?>
<?php
/*
	Classe/Função: exibe publicacoes
	Modificado: 04/02/2008
	Autor: TelosOnline.info by Heron Santos
*/
	$record_count = $mysql->record_count("capture_post_site","");
	if ($record_count > 0) {

		echo"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		echo"<!-- generator=\"TelosOnline.info v13\" -->\n";
		echo"<!-- sitemap-generator-url=\"http://www.telosonline.info\" sitemap-generator-version=\"1.0\"  -->\n";
		echo"<!-- Debug: Total comment count: ".$record_count." -->\n";
		echo"<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.84\">\n";
		echo"	<url>\n";
		echo"		<loc>http://www.telosonline.info/</loc>\n";
		echo"		<lastmod>".date("Y-m-d")."</lastmod>\n";
		echo"		<changefreq>daily</changefreq>\n";
		echo"		<priority>1</priority>\n";
		echo"	</url>\n";

		echo"<!-- Debug: Start Postings -->\n";
		$result = $mysql->get_sql("select id,public_date from capture_post_site where (id_site_list in(select site.id from capture_site_list site where (id_categoria_list in(select cat.id from capture_categoria_list cat where cat.id <> 5)))) order by id desc LIMIT 1000");
		while ($row = mysql_fetch_array($result)) {
			//$month = substr($row["public_date"], 5, 2);  
			//$date = substr($row["public_date"], 8, 2);  
			//$year = substr($row["public_date"], 0, 4);  
			//$hour = substr($row["public_time"],0,2);  
			//$minutes = substr($row["public_time"],3,2);  
			//$seconds = substr($row["public_time"],7,4);  
			//$public_date = date("Y-m-d\TG:i:s+00:00", mktime($hour,$minutes,$seconds,$month,$date,$year));

			echo"<!-- Debug: Priority report of postID ".$row['id']." -->\n";
			echo"	<url>\n";
			echo"		<loc>http://www.telosonline.info/?p=visualizar&amp;id=".$row['id']."</loc>\n";
			echo"		<lastmod>".$row["public_date"]."</lastmod>\n";
			echo"		<changefreq>weekly</changefreq>\n";
			echo"		<priority>0.1</priority>\n";
			echo"	</url>\n";
		}				

		echo"	<url>\n";
		echo"		<loc>http://www.telosonline.info/?m=notasgerais</loc>\n";
		echo"		<lastmod>2008-02-08</lastmod>\n";
		echo"		<changefreq>monthly</changefreq>\n";
		echo"		<priority>0.6</priority>\n";
		echo"	</url>\n";
		echo"<!-- Debug: End Postings -->\n";
	
		echo"<!-- Debug: Start Cats -->\n";
		echo"	<url>\n";
		echo"		<loc>http://www.telosonline.info/?p=publicacoes</loc>\n";
		echo"		<lastmod>".date("Y-m-d")."</lastmod>\n";
		echo"		<changefreq>daily</changefreq>\n";
		echo"		<priority>0.5</priority>\n";
		echo"	</url>\n";

		echo"	<url>\n";
		echo"		<loc>http://www.telosonline.info/?p=publicacoes&amp;categorias=1</loc>\n";
		echo"		<lastmod>".date("Y-m-d")."</lastmod>\n";
		echo"		<changefreq>daily</changefreq>\n";
		echo"		<priority>0.5</priority>\n";
		echo"	</url>\n";
		echo"	<url>\n";
		echo"		<loc>http://www.telosonline.info/?p=publicacoes&amp;categorias=2</loc>\n";
		echo"		<lastmod>".date("Y-m-d")."</lastmod>\n";
		echo"		<changefreq>daily</changefreq>\n";
		echo"		<priority>0.5</priority>\n";
		echo"	</url>\n";
		echo"	<url>\n";
		echo"		<loc>http://www.telosonline.info/?p=publicacoes&amp;categorias=3</loc>\n";
		echo"		<lastmod>".date("Y-m-d")."</lastmod>\n";
		echo"		<changefreq>daily</changefreq>\n";
		echo"		<priority>0.5</priority>\n";
		echo"	</url>\n";
		echo"	<url>\n";
		echo"		<loc>http://www.telosonline.info/?p=publicacoes&amp;categorias=4</loc>\n";
		echo"		<lastmod>".date("Y-m-d")."</lastmod>\n";
		echo"		<changefreq>daily</changefreq>\n";
		echo"		<priority>0.5</priority>\n";
		echo"	</url>\n";
		echo"	<url>\n";
		echo"		<loc>http://www.telosonline.info/?p=publicacoes&amp;categorias=5</loc>\n";
		echo"		<lastmod>".date("Y-m-d")."</lastmod>\n";
		echo"		<changefreq>daily</changefreq>\n";
		echo"		<priority>0.5</priority>\n";
		echo"	</url>\n";
		echo"	<url>\n";
		echo"		<loc>http://www.telosonline.info/?p=publicacoes&amp;categorias=6</loc>\n";
		echo"		<lastmod>".date("Y-m-d")."</lastmod>\n";
		echo"		<changefreq>daily</changefreq>\n";
		echo"		<priority>0.5</priority>\n";
		echo"	</url>\n";
		echo"<!-- Debug: End Cats -->\n";

		echo"<!-- Debug: Start Custom Pages -->\n";
		echo"<!-- Debug: End Custom Pages -->\n";
		echo"<!-- Debug: Start additional urls -->\n";
		echo"<!-- Debug: End additional urls -->\n";
		echo"</urlset>\n";
	}
?>