<?php require_once("./configurations.php"); ?>
<?php 
	function microtime_float(){
	if (version_compare(phpversion(), '5.0.0', '>=')){
		return microtime(true);
	} else {
		list($usec, $sec) = explode(' ', microtime());
		return ((float) $usec + (float) $sec);
	}
	}
	$start = microtime_float();

	header("Cache-Control: no-cache, must-revalidate");
	header("Content-Type: text/html; charset=utf-8",true);
	header("Pragma: no-cache");
?>
<?php include("./kernel/php-class/string.php"); ?>
<?php include("./kernel/php-function/get_date.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> -->
<title><?php echo $head_title ?></title>
<!-- estilos -->
	<style>
		@import "./kernel/style/general.css";
	</style>
<!-- javascrips -->
	<script type="text/javascript" src="kernel/jscript/fix_netscape.js"></script>
</head>

<body>
<?php 
/*
	Classe/Funo: capturador de feed
	Modificado: 13/02/2008
	Autor: TelosOnline.info by Heron Santos
*/
	//$sql_where_site_list = "";
	print_r("<br><strong><em>INICIANDO CAPTURACAO...</em></strong><br>\n");
	include("./kernel/extra/simplepie.inc");
	$mysql_cf = new MySQL;
	
	$sql_where_site_list = "view_posts = \"S\" and update_date <> \"".date("Y-m-d")."\"";
	//$sql_where_site_list = "view_posts = \"S\"";
	$record_count = $mysql_cf->record_count("capture_site_list",$sql_where_site_list);
	if ($record_count > 0):
		print_r("<br><em>Listando Feed's...</em><br>\n");
		
		$clear = new string;
		$result = $mysql_cf->get_sql("select * from capture_site_list where ". $sql_where_site_list);
		while ($capture_site_list = mysql_fetch_array($result)):
			print_r($capture_site_list["xml"]);
			$record_count--;
			$array_site_list .= $capture_site_list["xml"];
			if ($record_count > 0) {
				$array_site_list .= ";\n";
			}
			$mysql_cf->set_value("capture_site_list","update_date","'".date("Y-m-d")."'","id = ".$capture_site_list["id"]);
			print_r(" - <strong>LISTADO...</strong><br>\n");
		endwhile;
		$array = explode(';',$array_site_list);
		
		print_r("<br><em>Caregando Feed's...</em><br>\n");
		
		$feed = new SimplePie();
		$feed->set_feed_url($array);
		$feed->init();
		$feed->handle_content_type();
	
		if ($feed->error):
		echo $feed->error();
		endif;
		
		foreach($feed->get_items() as $item):
			$feed = $item->get_feed();

			$id_site_list = $mysql_cf->get_value("capture_site_list","id","xml=\"".$feed->subscribe_url()."\"");
			$public_date = $item->get_date('Y-m-d');
			$public_time = $item->get_date('H:i:s');

			if (empty($public_date)) $public_date = date('Y-m-d');
			if (empty($public_time)) $public_time = date('H:i:s');

			if ($public_date <= '2001-01-01' || $public_date > date('Y-m-d')) $public_date = date('Y-m-d');

			$array_feed[] = array('id_site_list' => $id_site_list, 'titulo' => $clear->get_clear($item->get_title()), 'descricao' => $clear->get_clear($item->get_content()), 'url' => $clear->get_clear_url($item->get_permalink()), 'public_date' => $public_date, 'public_time' => $public_time);
		endforeach;
		
		print_r("<br><em>Cadastrando/atualizando Feed's...</em><br>\n");
		
		$insert = $mysql_cf->get_sql("SET CHARACTER SET 'utf8'");
		foreach($array_feed as $row):
			$update_or_insert = 0;
			$update_or_insert = $mysql_cf->record_count("capture_post_site","(id_site_list = ". $row["id_site_list"] .") and lower(url) = lower(\"". $row["url"] ."\") or lower(titulo) like lower(\"%".$row["titulo"]."%\")");
			if ($update_or_insert == 0):
				if (!empty($row["id_site_list"]) || !empty($row["titulo"]) || $row["titulo"] != "" || $row["titulo"] != " "):
					$sql_insert = "insert into capture_post_site set ";
					$sql_insert .= "id_site_list = ". $row["id_site_list"] .",";
					$sql_insert .= "titulo = \"". $row["titulo"] ."\",";
					$sql_insert .= "descricao = \"". $row["descricao"] ."\",";
					$sql_insert .= "url = \"". $row["url"] ."\",";
					$sql_insert .= "public_date = \"". $row["public_date"] ."\",";
					$sql_insert .= "public_time = \"". $row["public_time"] ."\",";
					$sql_insert .= "nota_destaque = 0,";
					$sql_insert .= "page_views = 0";
					$insert = $mysql_cf->get_sql($sql_insert);
					print_r($row["public_date"] . " - " . $row["titulo"] . "<br>\n");
				endif;
			endif;
		endforeach;
		
		print_r("<br><strong><em>Concluido em ". round(microtime_float()-$start, 3) ."s!</em></strong>");
	endif;
?>
</body>
</html>