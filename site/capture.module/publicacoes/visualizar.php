<style>

	@import "./kernel/style/publicacoes.visualizar.css";

</style>

<table cellspacing="0" id="icone">

	<tr>

		<td class="imagem"><img src="kernel/image/corpo.titulo.icone.gif" alt="" /></td>

		<td class="texto">

			<a href="?">publica&ccedil;&otilde;es</a>

			<a href="?p=publicacoes<?php echo $link_filtros . $link_listagens ?>"><?php echo $referencia_categorias ?></a>

			<a href="?p=publicacoes<?php echo $link_categorias.$link_listagens ?>"><?php echo $referencia_filtros ?></a>

			<a href="?p=publicacoes<?php echo $link_categorias.$link_filtros ?>"><?php echo $referencia_listagens ?></a>

			<?php echo substr($referencia_visualizar,0,78) . "..." ?>

		</td>

	</tr>

</table>

<table cellspacing="0" id="visualizar">

	<tr>

		<td class="espaco"></td>

		<td class="titulo"><?php echo $titulo_publicacao ?></td>

		<td class="espaco"></td>

	</tr>

	<tr>

		<td class="espaco"></td>

		<td class="descricao">

		<div id=HOTWordsTxt name=HOTWordsTxt><?php echo $descricao_publicacao ?></div></td>

		<td class="espaco"></td>

	</tr>

<?php 

	if ($site_view_from_site_publicacao == "N") {

?>

	<tr>

		<td class="barra"></td>

		<td class="naointegra"><a href="?url=<?php echo $cripto->hide($url_publicacao) ?>&id=<?php echo $cripto->hide($id_publicacao) ?>" target="_blank"><font color="#000000">o autor n&atilde;o autorizou a publica&ccedil;&atilde;o do texto na &iacute;ntegra...</font><br /><strong>clique aqui, para visualiz&aacute;-lo em seu local de origem...</strong></a></td>

		<td class="barra"></td>

	</tr>

<?php 

	} elseif ($site_view_from_site_publicacao == "S") {

?>

	<tr>

		<td class="barra"></td>

		<td class="integra"><div id=HOTWordsTxt name=HOTWordsTxt><?php echo $integra_publicacao[0] ?></div></td>

		<td class="barra"></td>

	</tr>

<?php 

	}

?>

	<tr>

		<td class="espaco"></td>

		<td class="rodape">

			Publicado em <strong><?php echo ucfirst(substr($public_date_publicacao, 8, 2)." de ".conversorextenso(substr($public_date_publicacao, 5, 2), "num.mes")." de ".substr($public_date_publicacao, 0, 4)) ?></strong> &agrave;s <strong><?php echo $public_time_publicacao ?></strong>, oriundo do site <a href="?url=<?php echo $cripto->hide($site_url_publicacao) ?>" target="_blank"><strong><?php echo $site_nome_publicacao ?></strong></a>.<br />

			Esta publica&ccedil;&atilde;o foi  visualizada <strong><?php echo $page_views_publicacao ?></strong> vezes, obtendo um destaque de <strong><?php echo $nota_destaque_publicacao ?></strong>%.<br />

			Lembramos que o texto &eacute; de responsabilidade de seu(s) autor(es), onde poder&atilde;o ter seus direitos reservados. 

		</td>

		<td class="espaco"></td>

	</tr>

</table>