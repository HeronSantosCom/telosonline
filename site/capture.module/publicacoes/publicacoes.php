<style>
	@import "./kernel/style/publicacoes.publicacoes.css";
</style>
<table cellspacing="0" id="icone">
	<tr>
		<td class="imagem"><img src="kernel/image/corpo.titulo.icone.gif" alt="" /></td>
		<td class="texto">
			<a href="?">publica&ccedil;&otilde;es</a>
			<a href="?p=publicacoes<?php echo $link_filtros . $link_listagens ?>"><?php echo $referencia_categorias ?></a>
			<a href="?p=publicacoes<?php echo $link_categorias.$link_listagens ?>"><?php echo $referencia_filtros ?></a>
			<a href="?p=publicacoes<?php echo $link_categorias.$link_filtros ?>"><?php echo $referencia_listagens ?></a>
		</td>
	</tr>
</table>
<table cellspacing="0" id="publicacoes">
	<tr>
		<td class="barra"></td>
		<td class="selecao">
			<img src="kernel/image/topo.baixo.menu.categorias.gif" alt="Categorias" />
			<img src="kernel/image/container.spacer.gif" width="5" alt="" />
			<?php 
				if ($categorias == "1") {
					$link_cat1 = $link_filtros.$link_listagens;
					$class_cat1 = "class=\"selecionado\"";
				} else {
					$link_cat1 = "&categorias=1".$link_filtros.$link_listagens;
					$class_cat1 = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_cat1 ?>"><img <?php echo $class_cat1 ?> src="kernel/image/topo.baixo.menu.categoria.analise.gif" alt="An&aacute;lises e Reviews" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($categorias == "2") {
					$link_cat2 = $link_filtros.$link_listagens;
					$class_cat2 = "class=\"selecionado\"";
				} else {
					$link_cat2 = "&categorias=2".$link_filtros.$link_listagens;
					$class_cat2 = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_cat2 ?>"><img <?php echo $class_cat2 ?> src="kernel/image/topo.baixo.menu.categoria.artigos.gif" alt="Artigos" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($categorias == "3") {
					$link_cat3 = $link_filtros.$link_listagens;
					$class_cat3 = "class=\"selecionado\"";
				} else {
					$link_cat3 = "&categorias=3".$link_filtros.$link_listagens;
					$class_cat3 = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_cat3 ?>"><img <?php echo $class_cat3 ?> src="kernel/image/topo.baixo.menu.categoria.bastidores.gif" alt="Bastidores" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($categorias == "4") {
					$link_cat4 = $link_filtros.$link_listagens;
					$class_cat4 = "class=\"selecionado\"";
				} else {
					$link_cat4 = "&categorias=4".$link_filtros.$link_listagens;
					$class_cat4 = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_cat4 ?>"><img <?php echo $class_cat4 ?> src="kernel/image/topo.baixo.menu.categoria.dicas.gif" alt="Dicas" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($categorias == "5") {
					$link_cat5 = $link_filtros.$link_listagens;
					$class_cat5 = "class=\"selecionado\"";
				} else {
					$link_cat5 = "&categorias=5".$link_filtros.$link_listagens;
					$class_cat5 = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_cat5 ?>"><img <?php echo $class_cat5 ?> src="kernel/image/topo.baixo.menu.categoria.noticias.gif" alt="Not&iacute;cias" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($categorias == "6") {
					$link_cat6 = $link_filtros.$link_listagens;
					$class_cat6 = "class=\"selecionado\"";
				} else {
					$link_cat6 = "&categorias=6".$link_filtros.$link_listagens;
					$class_cat6 = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_cat6 ?>"><img <?php echo $class_cat6 ?> src="kernel/image/topo.baixo.menu.categoria.tutoriais.gif" alt="Tutoriais" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($module_page == "publicacoes" && empty($categorias)) {
					$class_cat = "class=\"selecionado\"";
				} else {
					$class_cat = "";
				}
			?>
			<a href="?p=publicacoes"><img <?php echo $class_cat ?> src="kernel/image/topo.baixo.menu.categoria.todos2.gif" alt="Exibir todas as publica&ccedil;&otilde;es" /></a>
		</td>
		<td class="barra"></td>
	</tr>
	<tr>
		<td class="barra"></td>
		<td class="selecao">
			<img src="kernel/image/topo.baixo.menu.filtros.gif" alt="Filtros" />
			<img src="kernel/image/container.spacer.gif" width="5" alt="" />
			<?php 
				if ($filtros == "apple") {
					$link_apple = $link_categorias.$link_listagens;
					$class_apple = "class=\"selecionado\"";
				} else {
					$link_apple = $link_categorias."&filtros=apple".$link_listagens;
					$class_apple = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_apple ?>"><img <?php echo $class_apple ?> src="kernel/image/topo.baixo.menu.filtros.apple.gif" alt="Apple" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($filtros == "gadgets") {
					$link_gadgets = $link_categorias.$link_listagens;
					$class_gadgets = "class=\"selecionado\"";
				} else {
					$link_gadgets = $link_categorias."&filtros=gadgets".$link_listagens;
					$class_gadgets = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_gadgets ?>"><img <?php echo $class_gadgets ?> src="kernel/image/topo.baixo.menu.filtros.gadgets.gif" alt="Gadgets" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($filtros == "games") {
					$link_games = $link_categorias.$link_listagens;
					$class_games = "class=\"selecionado\"";
				} else {
					$link_games = $link_categorias."&filtros=games".$link_listagens;
					$class_games = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_games ?>"><img <?php echo $class_games ?> src="kernel/image/topo.baixo.menu.filtros.games.gif" alt="Games" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($filtros == "hardware") {
					$link_hardware = $link_categorias.$link_listagens;
					$class_hardware = "class=\"selecionado\"";
				} else {
					$link_hardware = $link_categorias."&filtros=hardware".$link_listagens;
					$class_hardware = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_hardware ?>"><img <?php echo $class_hardware ?> src="kernel/image/topo.baixo.menu.filtros.hardware.gif" alt="Hardware" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($filtros == "linux") {
					$link_linux = $link_categorias.$link_listagens;
					$class_linux = "class=\"selecionado\"";
				} else {
					$link_linux = $link_categorias."&filtros=linux".$link_listagens;
					$class_linux = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_linux ?>"><img <?php echo $class_linux ?> src="kernel/image/topo.baixo.menu.filtros.linux.gif" alt="Linux" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($filtros == "windows") {
					$link_windows = $link_categorias.$link_listagens;
					$class_windows = "class=\"selecionado\"";
				} else {
					$link_windows = $link_categorias."&filtros=windows".$link_listagens;
					$class_windows = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_windows ?>"><img <?php echo $class_windows ?> src="kernel/image/topo.baixo.menu.filtros.windows.gif" alt="windows" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if (!($filtros == "apple" or $filtros == "gadgets" or $filtros == "games" or $filtros == "hardware" or $filtros == "linux" or $filtros == "windows")) {
					$link_outros = $link_categorias.$link_listagens;
					$class_outros = "class=\"selecionado\"";
				} else {
					$link_outros = $link_categorias."&filtros=".$filtros.$link_listagens;
					$class_outros = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_outros ?>&filtros=<?php echo $filtros ?>"><img <?php echo $class_outros ?> src="kernel/image/topo.baixo.menu.filtros.outros.gif" alt="<?php echo $filtros ?>" /></a>
		</td>
		<td class="barra"></td>
	</tr>
	<tr>
		<td class="barra"></td>
		<td class="selecao">
			<img src="kernel/image/topo.baixo.menu.listagens.gif" alt="Listagens" />
			<img src="kernel/image/container.spacer.gif" width="5" alt="" />
			<?php 
				if ($listagens == "destaques") {
					$link_destaques = $link_categorias.$link_filtros;
					$class_destaques = "class=\"selecionado\"";
				} else {
					$link_destaques = $link_categorias.$link_filtros."&listagens=destaques";
					$class_destaques = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_destaques ?>"><img <?php echo $class_destaques ?> src="kernel/image/topo.baixo.menu.listagens.destaques.gif" alt="Destaques" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($listagens == "populares") {
					$link_populares = $link_categorias.$link_filtros;
					$class_populares = "class=\"selecionado\"";
				} else {
					$link_populares = $link_categorias.$link_filtros."&listagens=populares";
					$class_populares = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_populares ?>"><img <?php echo $class_populares ?> src="kernel/image/topo.baixo.menu.listagens.populares.gif" alt="Populares" /></a>
			<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
			<?php 
				if ($listagens == "novidades") {
					$link_novidades = $link_categorias.$link_filtros;
					$class_novidades = "class=\"selecionado\"";
				} else {
					$link_novidades = $link_categorias.$link_filtros."&listagens=novidades";
					$class_novidades = "";
				}
			?>
			<a href="?p=publicacoes<?php echo $link_novidades ?>"><img <?php echo $class_novidades ?> src="kernel/image/topo.baixo.menu.listagens.novidades.gif" alt="Novidades" /></a>
		</td>
		<td class="barra"></td>
	</tr>
<?php
/*
	Classe/Função: exibe publicacoes
	Modificado: 04/02/2008
	Autor: TelosOnline.info by Heron Santos
*/
	$record_count = $mysql->record_count("capture_post_site","$sql_where");
	if ($record_count > 0) {
		$sql_limit = 15;
	
		$pagina = $_GET["pagina"];
		if (!isset($pagina)) {
			$pagina = 1;
		}
		
		$sql_limit_init = ($pagina-1) * $sql_limit;

		$exibe = ceil($record_count/$sql_limit);
		$exibe++;

		if (!empty($sql_where)) $sql_where = "where ".$sql_where;
		if (!empty($sql_order)) $sql_order = "order by ".$sql_order;
		$result = $mysql->get_sql("select * from capture_post_site ".$sql_where." ".$sql_order." limit $sql_limit_init, $sql_limit");
		while ($capture_post_site = mysql_fetch_array($result)) {
			$array[] = array('destaque_id' => $capture_post_site["id"], 'destaque_titulo' => $capture_post_site["titulo"], 'destaque_descricao' => $capture_post_site["descricao"]);
		}
?>
	<tr>
		<td class="barra"></td>
		<td class="principal">
			<div id=HOTWordsTxt name=HOTWordsTxt>
			<?php 
				foreach ($array as $key => $row) {
					$array_id = $row['destaque_id'];
					$array_titulo = $row['destaque_titulo'];
					$array_descricao = $row['destaque_descricao'];
					echo "<a href=\"?p=visualizar".$link_categorias.$link_filtros.$link_listagens."&id=".$array_id."\">\n";
					echo "	<p><strong>".$array_titulo."</strong></p>\n";
					echo "	<p class=\"descricao\">".$array_descricao."</p>\n";
					echo "</a>\n";
				}
			?>
			</div>
		</td>
		<td class="barra"></td>
	</tr>
</table>
<table cellspacing="0" id="navegador">
	<tr>
		<td class="espaco"></td>
		<td class="titulo">Exibindo registros <span class="destacado"><?php echo ($sql_limit_init+1) ?></span> a <span class="destacado"><?php if ($sql_limit_init + $sql_limit <= $record_count) { echo ($sql_limit_init + $sql_limit); } else { echo $record_count; } ?></span>, de um total de <span class="destacado"><?php echo $record_count ?></span></td>
		<td class="espaco"></td>
	</tr>
	<tr>
		<td class="barra"></td>
		<td class="principal">
			<?php 
				if ($pagina > 1) {
					echo "<a href=\"?p=publicacoes".$link_categorias.$link_filtros.$link_listagens."&pagina=".($pagina-1)."\"><strong>&lt; p&aacute;gina anterior</strong></a>";
				} else {
					echo "&lt; p&aacute;gina anterior";
				}
				
				echo " | ";

				$painel = "";
				for ($x=1; $x<=$exibe; $x++) {
					if ($x>=$pagina-5 && $x<=$pagina-1) {
						$painel .= " <a href=\"?p=publicacoes".$link_categorias.$link_filtros.$link_listagens."&pagina=".$x."\">$x</a> ";
					}
					if ($x==$pagina) {
						$painel .= " <strong>[$x]</strong> ";
					}
					if ($x+1<=$exibe && ($x>=$pagina+1 && $x<=$pagina+5)) {
						$painel .= " <a href=\"?p=publicacoes".$link_categorias.$link_filtros.$link_listagens."&pagina=".$x."\">$x</a> ";
					}
				}
				echo $painel;

				echo " | ";

				if (($pagina+1) < $exibe) {
					echo "<a href=\"?p=publicacoes".$link_categorias.$link_filtros.$link_listagens."&pagina=".($pagina+1)."\"><strong>p&aacute;gina seguinte &gt;</strong></a>";
				} else {
					echo "p&aacute;gina seguinte &gt;";
				}
			?>
		</td>
		<td class="barra"></td>
	</tr>
</table>
<?php 
	} else {
?>
	<tr>
		<td class="barra"></td>
		<td class="erro" height="150">
			<p>n&atilde;o foi encontrado nenhuma publica&ccedil;&atilde;o na categoria selecionada...</p>
			<p><a href="<?php echo $_SERVER['HTTP_REFERER'] ?>"><strong>clique aqui para retornar...</strong></a></p>
		</td>
		<td class="barra"></td>
	</tr>
</table>
<?php 
	}
?>
<script>
	if(typeof(urchinTracker)!='function')document.write('<sc'+'ript src="'+
	'http'+(document.location.protocol=='https:'?'s://ssl':'://www')+
	'.google-analytics.com/urchin.js'+'"></sc'+'ript>')
	</script>
	<script>
	_uacct = 'UA-5231030-2';
	urchinTracker("/4081533452/test");
</script>
