<style>
	@import "./kernel/style/rssfeed.index.css";
</style>
<table cellspacing="0" id="icone">
	<tr>
		<td class="imagem"><img src="kernel/image/corpo.titulo.icone.gif" alt="" /></td>
		<td class="texto">
			<a href="?">publica&ccedil;&otilde;es</a> / rss feed
		</td>
	</tr>
</table>
<table cellspacing="0" id="rssfeed">
	<tr>
		<td class="espaco"></td>
		<td class="titulo">rss <span class="destacado">feed</span></td>
		<td class="espaco"></td>
	</tr>
	<tr>
		<td class="barra"></td>
		<td class="principal">
			<p class="titulo">O que &eacute; RSS?</p>
			<p>Trata-se de uma tecnologia para  distribui&ccedil;&atilde;o de informa&ccedil;&otilde;es na Internet, normalmente utilizado por  sites de not&iacute;cia e conte&uacute;do. Usando esta tecnologia, voc&ecirc; &eacute; informado  imediatamente sobre qualquer nova publica&ccedil;&atilde;o de conte&uacute;do ocorrida no  site, sem a necessidade de visit&aacute;-lo.</p>
			<p class="titulo">Como acess&aacute;-los?</p>
			<p>Os feeds do <strong>TelosOnline.info</strong> podem ser acessados com o uso de programas ou sites agregadores.</p>
			<p>Atrav&eacute;s dos bot&otilde;es abaixo voc&ecirc; poder&aacute; acessar feeds espec&iacute;ficos de  cada categoria, ou um feed geral, que engloba todas elas.</p>
			<fieldset class="selecao">
					<img src="kernel/image/topo.baixo.menu.categorias.gif" alt="Categorias" />
					<img src="kernel/image/container.spacer.gif" width="5" alt="" />
					<?php 
						if ($categorias == "1") {
							$link_cat1 = $link_filtros.$link_listagens;
							$class_cat1 = "class=\"selecionado\"";
						} else {
							$link_cat1 = "&categorias=1".$link_filtros.$link_listagens;
							$class_cat1 = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_cat1 ?>"><img <?php echo $class_cat1 ?> src="kernel/image/topo.baixo.menu.categoria.analise.gif" alt="An&aacute;lises e Reviews" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if ($categorias == "2") {
							$link_cat2 = $link_filtros.$link_listagens;
							$class_cat2 = "class=\"selecionado\"";
						} else {
							$link_cat2 = "&categorias=2".$link_filtros.$link_listagens;
							$class_cat2 = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_cat2 ?>"><img <?php echo $class_cat2 ?> src="kernel/image/topo.baixo.menu.categoria.artigos.gif" alt="Artigos" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if ($categorias == "3") {
							$link_cat3 = $link_filtros.$link_listagens;
							$class_cat3 = "class=\"selecionado\"";
						} else {
							$link_cat3 = "&categorias=3".$link_filtros.$link_listagens;
							$class_cat3 = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_cat3 ?>"><img <?php echo $class_cat3 ?> src="kernel/image/topo.baixo.menu.categoria.bastidores.gif" alt="Bastidores" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if ($categorias == "4") {
							$link_cat4 = $link_filtros.$link_listagens;
							$class_cat4 = "class=\"selecionado\"";
						} else {
							$link_cat4 = "&categorias=4".$link_filtros.$link_listagens;
							$class_cat4 = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_cat4 ?>"><img <?php echo $class_cat4 ?> src="kernel/image/topo.baixo.menu.categoria.dicas.gif" alt="Dicas" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if ($categorias == "5") {
							$link_cat5 = $link_filtros.$link_listagens;
							$class_cat5 = "class=\"selecionado\"";
						} else {
							$link_cat5 = "&categorias=5".$link_filtros.$link_listagens;
							$class_cat5 = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_cat5 ?>"><img <?php echo $class_cat5 ?> src="kernel/image/topo.baixo.menu.categoria.noticias.gif" alt="Not&iacute;cias" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if ($categorias == "6") {
							$link_cat6 = $link_filtros.$link_listagens;
							$class_cat6 = "class=\"selecionado\"";
						} else {
							$link_cat6 = "&categorias=6".$link_filtros.$link_listagens;
							$class_cat6 = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_cat6 ?>"><img <?php echo $class_cat6 ?> src="kernel/image/topo.baixo.menu.categoria.tutoriais.gif" alt="Tutoriais" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if (empty($categorias)) {
							$class_cat = "class=\"selecionado\"";
						} else {
							$class_cat = "";
						}
					?>
					<a href="?m=rssfeed"><img <?php echo $class_cat ?> src="kernel/image/topo.baixo.menu.categoria.todos2.gif" alt="Exibir todas as publica&ccedil;&otilde;es" /></a><br />
					<img src="kernel/image/topo.baixo.menu.filtros.gif" alt="Filtros" />
					<img src="kernel/image/container.spacer.gif" width="5" alt="" />
					<?php 
						if ($filtros == "apple") {
							$link_apple = $link_categorias.$link_listagens;
							$class_apple = "class=\"selecionado\"";
						} else {
							$link_apple = $link_categorias."&filtros=apple".$link_listagens;
							$class_apple = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_apple ?>"><img <?php echo $class_apple ?> src="kernel/image/topo.baixo.menu.filtros.apple.gif" alt="Apple" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if ($filtros == "gadgets") {
							$link_gadgets = $link_categorias.$link_listagens;
							$class_gadgets = "class=\"selecionado\"";
						} else {
							$link_gadgets = $link_categorias."&filtros=gadgets".$link_listagens;
							$class_gadgets = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_gadgets ?>"><img <?php echo $class_gadgets ?> src="kernel/image/topo.baixo.menu.filtros.gadgets.gif" alt="Gadgets" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if ($filtros == "games") {
							$link_games = $link_categorias.$link_listagens;
							$class_games = "class=\"selecionado\"";
						} else {
							$link_games = $link_categorias."&filtros=games".$link_listagens;
							$class_games = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_games ?>"><img <?php echo $class_games ?> src="kernel/image/topo.baixo.menu.filtros.games.gif" alt="Games" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if ($filtros == "hardware") {
							$link_hardware = $link_categorias.$link_listagens;
							$class_hardware = "class=\"selecionado\"";
						} else {
							$link_hardware = $link_categorias."&filtros=hardware".$link_listagens;
							$class_hardware = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_hardware ?>"><img <?php echo $class_hardware ?> src="kernel/image/topo.baixo.menu.filtros.hardware.gif" alt="Hardware" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if ($filtros == "linux") {
							$link_linux = $link_categorias.$link_listagens;
							$class_linux = "class=\"selecionado\"";
						} else {
							$link_linux = $link_categorias."&filtros=linux".$link_listagens;
							$class_linux = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_linux ?>"><img <?php echo $class_linux ?> src="kernel/image/topo.baixo.menu.filtros.linux.gif" alt="Linux" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if ($filtros == "windows") {
							$link_windows = $link_categorias.$link_listagens;
							$class_windows = "class=\"selecionado\"";
						} else {
							$link_windows = $link_categorias."&filtros=windows".$link_listagens;
							$class_windows = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_windows ?>"><img <?php echo $class_windows ?> src="kernel/image/topo.baixo.menu.filtros.windows.gif" alt="windows" /></a><br />
					<img src="kernel/image/topo.baixo.menu.listagens.gif" alt="Listagens" />
					<img src="kernel/image/container.spacer.gif" width="5" alt="" />
					<?php 
						if ($listagens == "destaques") {
							$link_destaques = $link_categorias.$link_filtros;
							$class_destaques = "class=\"selecionado\"";
						} else {
							$link_destaques = $link_categorias.$link_filtros."&listagens=destaques";
							$class_destaques = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_destaques ?>"><img <?php echo $class_destaques ?> src="kernel/image/topo.baixo.menu.listagens.destaques.gif" alt="Destaques" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if ($listagens == "populares") {
							$link_populares = $link_categorias.$link_filtros;
							$class_populares = "class=\"selecionado\"";
						} else {
							$link_populares = $link_categorias.$link_filtros."&listagens=populares";
							$class_populares = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_populares ?>"><img <?php echo $class_populares ?> src="kernel/image/topo.baixo.menu.listagens.populares.gif" alt="Populares" /></a>
					<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
					<?php 
						if ($listagens == "novidades") {
							$link_novidades = $link_categorias.$link_filtros;
							$class_novidades = "class=\"selecionado\"";
						} else {
							$link_novidades = $link_categorias.$link_filtros."&listagens=novidades";
							$class_novidades = "";
						}
					?>
					<a href="?m=rssfeed<?php echo $link_novidades ?>"><img <?php echo $class_novidades ?> src="kernel/image/topo.baixo.menu.listagens.novidades.gif" alt="Novidades" /></a>
			</fieldset>
			<fieldset>
				<legend>Resultado</legend>
				<label for="mensagem">Copie e cole o link abaixo no programa ou site agregador de feed:</label><textarea rows="3" name="mensagem" id="mensagem">http://www.telosonline.info/rss.php?<?php echo $link_categorias.$link_filtros.$link_listagens ?></textarea>
			</fieldset>
		</td>
		<td class="barra"></td>
	</tr>
</table>
<script>
	if(typeof(urchinTracker)!='function')document.write('<sc'+'ript src="'+
	'http'+(document.location.protocol=='https:'?'s://ssl':'://www')+
	'.google-analytics.com/urchin.js'+'"></sc'+'ript>')
	</script>
	<script>
	_uacct = 'UA-5231030-2';
	urchinTracker("/4081533452/goal");
</script>
