<?php require_once("./configurations.php"); ?>
<?php 
	function microtime_float(){
		if (version_compare(phpversion(), '5.0.0', '>=')){
			return microtime(true);
		} else {
			list($usec, $sec) = explode(' ', microtime());
			return ((float) $usec + (float) $sec);
		}
	}
	$start = microtime_float();

	header("Cache-Control: no-cache, must-revalidate");
	header("Content-Type: text/html; charset=utf-8",true);
	header("Pragma: no-cache");
?>
<?php include("./kernel/php-class/string.php"); ?>
<?php include("./kernel/php-function/redirect.php"); ?>
<?php include("./kernel/php-function/load_module.php"); ?>
<?php include("./kernel/php-function/get_date.php"); ?>
<?php include("./kernel/php-function/get_navigation_bar.php"); ?>
<?php include("./kernel/php-function/get_post.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?php echo $head_title ?></title>
<!-- descritores do site -->
	<meta name="description" content="<?php echo $head_description ?>" />
	<meta name="keywords" content="<?php echo $head_keyword ?>" />
	<meta name="author" content="root"/>
	<meta name="copyright" content="<?php echo $head_copyright ?>">
	<meta name="robots" content="index,follow">
	<meta name="revisit-after" content="1 days">
	<meta name="resource-type" content="document">
	<meta name="distribution" content="global">
	<meta name="rating" content="general">
	<meta http-equiv="Expires" content="0">
<!-- configuracao de indexadores -->
	<meta name="verify-v1" content="Nfr3D4XfZLgI/gTDtMR1pTkRE5jWO+C7PTGICXdPuoU=" />
	<meta name="y_key" content="18ea0d0717d19e1f" >
<!-- links relativos -->
	<link rel="shortcut icon" type="image/x-icon" href="http://www.telosonline.info/favicon.ico" />
	<link rel="alternate" type="application/rss+xml" title="<?php echo $head_title ?>" href="http://www.telosonline.info/rss.php?<?php echo $link_categorias.$link_filtros.$link_listagens ?>" />
	<link rel="search" type="application/opensearchdescription+xml" href="opensearch.xml" title="<?php echo $head_title ?>" />	
<!-- estilos -->
	<style>
		@import "./kernel/style/general.css";
		@import "./kernel/style/index.css";
	</style>
<!-- javascrips -->
	<script type="text/javascript" src="kernel/jscript/fix_netscape.js"></script>
</head>

<body>
<div id="oculto">
	<!-- In�cio do conte�do dispon�veis no site -->
	inform&aacute;tica, access 2007, access xp, autocad, excel 2000, excel 2007, excel xp, hardware,
	inform&aacute;tica, internet explorer, introdu&ccedil;&atilde;o a computa&ccedil;&atilde;o, linux, linux - ubuntu, ms project, open
	office calc, open office write, oracle, power point 2000, power point 2007, power point xp, redes,
	sql server, windows 98, windows me, windows server, windows vista, windows xp, word 2000,
	word 2007, word xp, programa&ccedil;&atilde;o, asp, cgi, delphi, dreamweaver, html, java, l&oacute;gica, php + mysql,
	php b&aacute;sico, vb .net, anima&ccedil;&otilde;es e design, 3d studio, corel draw, fireworks, flash, photoshop,
	profissionalizante, administra&ccedil;&atilde;o, administra&ccedil;&atilde;o do tempo, contabilidade, empreendedorismo, falar
	em p&uacute;blico, gest&atilde;o de conflitos, investimento, lideran&ccedil;a, motiva&ccedil;&atilde;o e produtividade, publicidade,
	t&eacute;cnicas de negocia&ccedil;&atilde;o, t&eacute;cnicas de vendas, telemarketing, l&iacute;ngua estrangeira, espanhol, franc&ecirc;s,
	ingl&ecirc;s, italiano, asp shopping cart code, asp shopping cart, asp shopping cart source code, asp
	shopping cart script, asp shopping cart tutorial, asp shopping cart software, asp shopping cart
	download, shopping cart code, shopping cart source code, code for shopping cart, shopping cart
	software, shopping cart, shopping cart cover, shopping cart covers, baby shopping cart cover, baby
	shopping cart covers, database shopping cart software, shopping cart in asp, php shopping cart code,
	php shopping cart, php shopping cart tutorial, asp cart, asp computer, asp baton, asp, asp hosting, asp
	pdf, asp create pdf, asp script, asp editor, edit asp, asp exif, asp code, asp designing, asp developer,
	asp development, asp programming, asp source, asp software, asp automation, asp billing, asp crm,
	asp invoicing, asp bulletin board, asp billing software, asp tools, asp application, php, learning php,
	php hosting, linux hosting php, php barcode, php barcodes, php auction script, php reporting, php
	auction, php auctions, php survey, php surveys, randomizer php, php auction scripts, php pdf server,
	pdf php, php asp, linux php, php job board, php security, flash php, php javascript, php vieraskirja,
	sql php, script in php, array php, date php, email php, file php, gallery php, include php, login php,
	pear php, php 4.1, php 4.2, php gd, php ide, php programming, php shopping, php webspace, upload
	php, visual php, linux, debian linux, linux usb, linux web, firewall linux, cheap linux, linux
	computer, linux computers, eda linux, linux shop, linux versand, linux firewall software, linux
	reseller hosting, linux embedded, linux laptop, linux on, linux to, javascript editor linux, linux on
	laptop, linux book, linux books, embedded linux computer, linux programming tool, linux
	programming tools, linux tech support, arm linux embedded, best linux book, linux kernel book,
	linux web hosting, cheap web hosting, sql web hosting, php web hosting, curso de topografia, curso
	de italiano, curso de flash, curso de tecnologia, curso de macromedia flash, curso de ingl&ecirc;s, cursos de
	topografia, curso de 3d, curso de anima&ccedil;&atilde;o, cursos de flash, curso de tecnologia da informa&ccedil;&atilde;o, curso
	de comunica&ccedil;&atilde;o, curso de anima&ccedil;&atilde;o 3d, curso de ingl&ecirc;s no exterior, curso de dactilografia, curso de
	digita&ccedil;&atilde;o, cursos de ingl&ecirc;s, curso de ingles, curso de pintura em tela, asp sql hosting, php sql hosting,
	windows sql hosting, sql hosting, script, telemarketing script, auction script, randomizer script,
	online auction script, job board script, asp net obfuscator, protect asp net, asp net portal, asp net
	development, asp net file, asp net upload, asp net generator, asp net file upload, asp net code
	generator, asp net bulletin board, web asp net control, asp net document, asp net message board
	software, asp net email, job board php script, php script, php script tutorial, simple php script, debian
	hosting, php tutorial, tecnologia, vagas tecnologia, articoli tecnologia, gradua&ccedil;&atilde;o em tecnologia,
	curso italiano, delphi pdf, curso, debian host, debian vps, javascript menus tutorial, curso 3d,
	javascript
	<!-- Fim do conte�do dispon�veis no site -->
</div>
<div id="background">
	<table cellspacing="0" id="container">
		<tr>
			<td class="margem-esquerda">&nbsp;</td>
			<td class="container">
				<!-- in�cio do topo -->
					<table cellspacing="0" id="topo">
						<tr class="cima">
							<td colspan="2"><img src="kernel/image/topo.cima.logotipo.png" alt="TelosOnline.info" /></td>
						</tr>
						<tr class="baixo">
							<td class="aba">
								<?php 
									if ($module_name == "publicacoes") { $class_publicacoes = "selecionado"; } else { $class_publicacoes = "normal"; }
									if ($module_name == "cursos") { $class_cursos = "selecionado"; } else { $class_cursos = "normal"; }
									if ($module_name == "servicos") { $class_servicos = "selecionado"; } else { $class_servicos = "normal"; }
								?>
								<a href="?" class="<?php echo $class_publicacoes ?>"><img src="kernel/image/topo.baixo.aba.publicacoes.gif" alt="Publica&ccedil;&otilde;es" /></a>
								<a href="?url=<?php echo $cripto->hide("http://www.iped.com.br/cursos/telosonline") ?>" class="<?php echo $class_cursos ?>" target="_blank"><img src="kernel/image/topo.baixo.aba.cursos.gif" alt="Cursos Online" /></a>
								<!--<a href="?m=servicos" class="<?php echo $class_servicos ?>"><img src="kernel/image/topo.baixo.aba.servicos.gif" alt="Servi&ccedil;os" /></a>-->
							</td>
							<td class="logotipo"><img src="kernel/image/topo.baixo.logotipo.png" alt="Tecnologia levado a s�rio..." /></td>
						</tr>
					</table>
				<!-- fim do topo -->
				
				<!-- inicio do menu -->
					<table cellspacing="0" id="menu">
						<tr>
							<form action="?p=publicacoes" method="POST">
							<td class="cima">
								<a href="?p=publicacoes"><img <?php echo $class_cat ?> src="kernel/image/topo.baixo.menu.categoria.todos.gif" alt="Exibir todas as publica&ccedil;&otilde;es" /></a>
								<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
								<?php 
									if ($categorias == "1") {
										$link_cat1 = $link_filtros.$link_listagens;
										$class_cat1 = "class=\"selecionado\"";
									} else {
										$link_cat1 = "&categorias=1".$link_filtros.$link_listagens;
										$class_cat1 = "";
									}
								?>
								<a href="?p=publicacoes<?php echo $link_cat1 ?>"><img <?php echo $class_cat1 ?> src="kernel/image/topo.baixo.menu.categoria.analise.gif" alt="An&aacute;lises e Reviews" /></a>
								<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
								<?php 
									if ($categorias == "2") {
										$link_cat2 = $link_filtros.$link_listagens;
										$class_cat2 = "class=\"selecionado\"";
									} else {
										$link_cat2 = "&categorias=2".$link_filtros.$link_listagens;
										$class_cat2 = "";
									}
								?>
								<a href="?p=publicacoes<?php echo $link_cat2 ?>"><img <?php echo $class_cat2 ?> src="kernel/image/topo.baixo.menu.categoria.artigos.gif" alt="Artigos" /></a>
								<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
								<?php 
									if ($categorias == "3") {
										$link_cat3 = $link_filtros.$link_listagens;
										$class_cat3 = "class=\"selecionado\"";
									} else {
										$link_cat3 = "&categorias=3".$link_filtros.$link_listagens;
										$class_cat3 = "";
									}
								?>
								<a href="?p=publicacoes<?php echo $link_cat3 ?>"><img <?php echo $class_cat3 ?> src="kernel/image/topo.baixo.menu.categoria.bastidores.gif" alt="Bastidores" /></a>
								<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
								<?php 
									if ($categorias == "4") {
										$link_cat4 = $link_filtros.$link_listagens;
										$class_cat4 = "class=\"selecionado\"";
									} else {
										$link_cat4 = "&categorias=4".$link_filtros.$link_listagens;
										$class_cat4 = "";
									}
								?>
								<a href="?p=publicacoes<?php echo $link_cat4 ?>"><img <?php echo $class_cat4 ?> src="kernel/image/topo.baixo.menu.categoria.dicas.gif" alt="Dicas" /></a>
								<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
								<?php 
									if ($categorias == "5") {
										$link_cat5 = $link_filtros.$link_listagens;
										$class_cat5 = "class=\"selecionado\"";
									} else {
										$link_cat5 = "&categorias=5".$link_filtros.$link_listagens;
										$class_cat5 = "";
									}
								?>
								<a href="?p=publicacoes<?php echo $link_cat5 ?>"><img <?php echo $class_cat5 ?> src="kernel/image/topo.baixo.menu.categoria.noticias.gif" alt="Not&iacute;cias" /></a>
								<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
								<?php 
									if ($categorias == "6") {
										$link_cat6 = $link_filtros.$link_listagens;
										$class_cat6 = "class=\"selecionado\"";
									} else {
										$link_cat6 = "&categorias=6".$link_filtros.$link_listagens;
										$class_cat6 = "";
									}
								?>
								<a href="?p=publicacoes<?php echo $link_cat6 ?>"><img <?php echo $class_cat6 ?> src="kernel/image/topo.baixo.menu.categoria.tutoriais.gif" alt="Tutoriais" /></a>
								<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
								<?php 
									if ($module_page == "publicacoes" && empty($categorias)) {
										$class_cat = "class=\"selecionado\"";
									} else {
										$class_cat = "";
									}
								?>
								<input name="filtros" type="text" id="filtros" maxlength="255" value="<?php echo $filtros ?>" />
								<input type="image" src="kernel/image/buscador.cima.ok.gif" alt="ok" align="absmiddle" />
							</td>
							</form>
						</tr>
					</table>
				<!-- fim do menu -->
	
				<!-- inicio da publicidade 1 -->
					<table cellspacing="0" id="publicidade">
						<tr>
							<td class="cima">
								<!-- An�ncio Din�mico BuscaP� - IN�CIO -->
								<div style="width:728px; height:90px; overflow:hidden;">
								<div style="overflow:hidden;">
									<script language="javascript" src="http://ivitrine.buscape.com/bpadsvar.js"></script>
									<script language="javascript" >
									<!--
										buscapeads_afiliado = "11365";
										buscapeads_tipo_cliente = "2";
										buscapeads_vitrine_local = "BR";
										buscapeads_site_origem = "1288682";
										buscapeads_vitrine_vers = "1.23";
										buscapeads_formato = "728x90";
										buscapeads_id_keyword = "41826";
										buscapeads_txt_keyword = "";
										buscapeads_tipo_canal = "3";
										buscapeads_categorias = "1,2";
										buscapeads_excluir = "3";
										buscapeads_idioma = "pt";
										buscapeads_pais = "BR";
										buscapeads_area = "";
										buscapeads_estado = "";
										buscapeads_cidade = "";
										buscapeads_cep = "";
									//--></script>
									<script language="javascript" src="http://ivitrine.buscape.com/bpads.js"></script>
								</div>
								<!-- An�ncio Din�mico BuscaP� - FIM -->
							</td>
						</tr>
					</table>
				<!-- fim da publicidade 1 -->
	
				<!-- inicio da CONAPHP -->
					<table cellspacing="0" id="capture">
						<tr>
							<td class="cima">
								<a href="http://www.conaphp.com.br/" target="_blank"><img src="http://www.conaphp.com.br/graficos/banners/conaphp-transparente-468x60.gif" alt="CONAPHP - COngresso NAcional de PHP 2008" style="padding-top: 10px;"></a>
							</td>
						</tr>
					</table>
				<!-- fim da CONAPHP -->
	
				<!-- inicio do conteudo da pagina -->
					<?php require_once($opener); ?>
				<!-- fim do conteudo da pagina -->
				
				<!-- inicio feed capture -->
					<table cellspacing="0" id="capture">
						<tr>
							<td class="baixo">
								carregado em <?php echo round(microtime_float()-$start, 3); ?>s<br />
								<a href="http://dihitt.com.br?ref=14328" dihitt_check="7a9682355ec6a77081df95b05bde1748"><img src="http://dihitt.com.br/banners/dihitt_16x16_01.jpg"></a>
								<!-- Histats.com START -->
									<script type="text/javascript" language="JavaScript"> 
										var s_sid = 83881;
										var st_dominio = 4; 
										var cimg = 0;
										var cwi =150;
										var che =30; 
									</script>
									<script type="text/javascript" language="JavaScript" src="http://s10.histats.com/js9.js"></script>
									<noscript><a href="http://www.histats.com" target="_blank"><img src="http://s4.histats.com/stats/0.gif?83881&1" alt="free tracking"></a></noscript>
								<!-- Histats.com END -->
							</td>
						</tr>
					</table>
				<!-- fim feed capture -->
	
				<!-- inicio da publicidade 2 -->
					<table cellspacing="0" id="publicidade">
						<tr>
							<td class="baixo">
								<script type="text/javascript">
								<!--
									google_ad_client = "pub-7626566422060366";
									/* 728x90, criado 08/02/08 */
									google_ad_slot = "8228385608";
									google_ad_width = 728;
									google_ad_height = 90;
								//-->
								</script>
								<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
							</td>
						</tr>
					</table>
				<!-- fim da publicidade 2 -->
	
				<!-- inicio do rodape -->
					<table cellspacing="0" id="rodape">
						<tr>
							<td class="copyright"><img src="kernel/image/rodape.copyright.gif" alt="Copyright &copy; 2008 TelosOnline.info" /></td>
							<td class="links">
								<a href="?m=contato"><img src="kernel/image/rodape.contato.gif" alt="Contato" /></a>
								<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
								<a href="?m=notasgerais"><img src="kernel/image/rodape.notasgerais.gif" alt="Notas Gerais" /></a>
								<img src="kernel/image/topo.baixo.menu.barra.gif" alt="" />
								<a href="?m=rssfeed"><img src="kernel/image/rodape.rssfeed.gif" alt="RSS Feeds" /></a>
							</td>
						</tr>
					</table>
				<!-- fim do rodape -->
			</td>
			<td class="margem-direita">&nbsp;</td>
		</tr>
	</table>
</div>
<script src='http://ads6962.hotwords.com.br/show.jsp?id=6962'></script>
<!-- Start of Google Analytics -->
<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
	var pageTracker = _gat._getTracker("UA-5231030-3");
	pageTracker._trackPageview();
</script>
<!-- End of Google Analytics -->
</body>
</html>
