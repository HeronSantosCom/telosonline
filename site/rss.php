<?php require_once("./configurations.php"); ?>
<?php 
	header("Content-Type: text/xml; charset=iso-8859-1",true);
	header('Content-Disposition: inline; filename=rss.xml');
?>
<?php include("./kernel/php-function/get_date.php"); ?>
<?php include("./kernel/php-function/get_navigation_bar.php"); ?>
<?php
/*
	Classe/Função: exibe publicacoes
	Modificado: 04/02/2008
	Autor: TelosOnline.info by Heron Santos
*/
	$record_count = $mysql->record_count("capture_post_site","$sql_where");
	if ($record_count > 0) {
		$sql_limit = 20;

		if (!empty($sql_where)) $sql_where = "where ".$sql_where;
		if (!empty($sql_order)) $sql_order = "order by ".$sql_order;
		$result = $mysql->get_sql("select * from capture_post_site ".$sql_where." ".$sql_order." limit $sql_limit");
		while ($capture_post_site = mysql_fetch_array($result)) {
			$month = substr($capture_post_site["public_date"], 5, 2);  
			$date = substr($capture_post_site["public_date"], 8, 2);  
			$year = substr($capture_post_site["public_date"], 0, 4);  
			$hour = substr($capture_post_site["public_time"],0,2);  
			$minutes = substr($capture_post_site["public_time"],3,2);  
			$seconds = substr($capture_post_site["public_time"],7,4);  

			$public_date = date("D, j M Y G:i:s T", mktime($hour,$minutes,$seconds,$month,$date,$year));
			$array[] = array('destaque_id' => $capture_post_site["id"], 'destaque_titulo' => $capture_post_site["titulo"], 'destaque_descricao' => $capture_post_site["descricao"], 'destaque_public_date' => $public_date);
		}
		
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
		echo "<rss version=\"2.0\">\n";
		echo "	<channel>\n";
		echo "		<title>".$head_title."</title>\n";
		echo "		<link>http://www.telosonline.info/rss.php?p=publicacoes".$link_categorias.$link_listagens.$link_filtros."</link>\n";
		echo "		<description>".$head_description."</description>\n";
		echo "		<language>pt-br</language>\n";
		echo "		<copyright>".$head_copyright."</copyright>\n";
		echo "		<image>\n";
		echo "			<title>".$head_title."</title>\n";
		echo "			<width>129</width>\n";
		echo "			<height>34</height>\n";
		echo "			<link>http://www.telosonline.info/</link>\n";
		echo "			<url>http://www.telosonline.info/kernel/image/feed.logotipo.gif</url>\n";
		echo "		</image>\n";
		echo "		<webMaster>".$head_webmaster."</webMaster>\n";
		foreach ($array as $key => $row) {
			echo "		<item>\n";
			echo "			<title><![CDATA[".$row['destaque_titulo']."...]]></title>\n";
			//echo "			<title>".$row['destaque_titulo']."</title>\n";
			echo "			<link>http://www.telosonline.info/?p=visualizar".$link_categorias.$link_filtros.$link_listagens."&amp;id=".$row['destaque_id']."</link>\n";
			echo "			<guid>http://www.telosonline.info/?p=visualizar".$link_categorias.$link_filtros.$link_listagens."&amp;id=".$row['destaque_id']."</guid>\n";
			echo "			<description><![CDATA[".$row['destaque_descricao']."...]]></description>\n";
			//echo "			<description>".$row['destaque_descricao']."</description>\n";
			echo "			<pubDate>".$row['destaque_public_date']."</pubDate>\n";
			echo "		</item>\n";
		}
		echo "	</channel>\n";
		echo "</rss>\n";
	}
?>