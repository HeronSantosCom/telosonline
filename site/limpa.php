<?php require_once("./configurations.php"); ?>
<?php 
	function microtime_float(){
	if (version_compare(phpversion(), '5.0.0', '>=')){
		return microtime(true);
	} else {
		list($usec, $sec) = explode(' ', microtime());
		return ((float) $usec + (float) $sec);
	}
	}
	$start = microtime_float();

	header("Cache-Control: no-cache, must-revalidate");
	header("Content-Type: text/html; charset=utf-8",true);
	header("Pragma: no-cache");
?>
<?php include("./kernel/php-class/string.php"); ?>
<?php include("./kernel/php-function/get_date.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $head_title ?></title>
<!-- estilos -->
	<style>
		@import "./kernel/style/general.css";
	</style>
<!-- javascrips -->
	<script type="text/javascript" src="kernel/jscript/fix_netscape.js"></script>
</head>

<body>
<?php 
/*
	Classe/Funo: atualiza cadastros
	Modificado: 13/02/2008
	Autor: TelosOnline.info by Heron Santos
*/
	$clear = new string;
	
	print_r("<br><b>LIMPANDO CAMPOS</b><br>\n");
	$result = $mysql->get_sql("SELECT id,titulo,descricao,url FROM capture_post_site ORDER BY id ASC");
	while ($row = mysql_fetch_array($result)):
		print_r($row["id"] . "<br>\n");
		$mysql->set_value("capture_post_site","titulo","\"". $clear->get_clear($row["titulo"]) ."\"","id = " . $row["id"]);
		$mysql->set_value("capture_post_site","descricao","\"". $clear->get_clear($row["descricao"]) ."\"","id = " . $row["id"]);
		$mysql->set_value("capture_post_site","url","\"". $clear->get_clear_url($row["url"]) ."\"","id = " . $row["id"]);
	endwhile;

	print_r("<br><b>LIMPANDO TITULO NULO 1</b><br>\n");
	$result = $mysql->get_sql("SELECT id,titulo FROM capture_post_site WHERE titulo = \"\"");
	while ($row = mysql_fetch_array($result)):
		print_r($row["id"] . "<br>\n");
		$delete = $mysql->get_sql("DELETE FROM capture_post_site WHERE id = " . $row["id"]);
	endwhile;
	
	print_r("<br><b>LIMPANDO TITULO NULO 2</b><br>\n");
	$result = $mysql->get_sql("SELECT id,titulo FROM capture_post_site WHERE titulo is null");
	while ($row = mysql_fetch_array($result)):
		print_r($row["id"] . "<br>\n");
		$delete = $mysql->get_sql("DELETE FROM capture_post_site WHERE id = " . $row["id"]);
	endwhile;
	
	print_r("<br><b>LIMPANDO DESCRICAO NULO 1</b><br>\n");
	$result = $mysql->get_sql("SELECT id,descricao FROM capture_post_site WHERE descricao = \"\"");
	while ($row = mysql_fetch_array($result)):
		print_r($row["id"] . "<br>\n");
		$delete = $mysql->get_sql("DELETE FROM capture_post_site WHERE id = " . $row["id"]);
	endwhile;
	
	print_r("<br><b>LIMPANDO DESCRICAO NULO 2</b><br>\n");
	$result = $mysql->get_sql("SELECT id,descricao FROM capture_post_site WHERE descricao is null");
	while ($row = mysql_fetch_array($result)):
		print_r($row["id"] . "<br>\n");
		$delete = $mysql->get_sql("DELETE FROM capture_post_site WHERE id = " . $row["id"]);
	endwhile;

	print_r("<br><b>LIMPANDO DUPLICADOS</b><br>\n");
	$result = $mysql->get_sql("SELECT * FROM capture_post_site GROUP BY titulo HAVING Count(*) > 1");
	while ($row = mysql_fetch_array($result)):
		print_r($row["id"] . "<br>\n");
		$delete = $mysql->get_sql("DELETE FROM capture_post_site WHERE id = " . $row["id"]);
	endwhile;
	
	print_r("<br><strong><em>Concluido em ". round(microtime_float()-$start, 3) ."s!</em></strong>");
?>
</body>
</html>