<?php 
/*
	Classe/Função: carrega publicacao do site de origem
	Modificado: 04/02/2008
	Autor: TelosOnline.info by Heron Santos
*/
	if (!empty($id)) {
		$sql_where_publicacao = "id = $id";
		$id_publicacao = $mysql->get_value("capture_post_site","id",$sql_where_publicacao);
		$titulo_publicacao = $mysql->get_value("capture_post_site","titulo",$sql_where_publicacao);
		$descricao_publicacao = $mysql->get_value("capture_post_site","descricao",$sql_where_publicacao);
		$url_publicacao = $mysql->get_value("capture_post_site","url",$sql_where_publicacao);
		$public_date_publicacao = $mysql->get_value("capture_post_site","public_date",$sql_where_publicacao);
		$public_time_publicacao = $mysql->get_value("capture_post_site","public_time",$sql_where_publicacao);
		$nota_destaque_publicacao = $mysql->get_value("capture_post_site","nota_destaque",$sql_where_publicacao);
		$page_views_publicacao = $mysql->get_value("capture_post_site","page_views",$sql_where_publicacao);
		$id_site_list_publicacao = $mysql->get_value("capture_post_site","id_site_list",$sql_where_publicacao);

		$sql_where_site = "id=$id_site_list_publicacao";
		$site_nome_publicacao = $mysql->get_value("capture_site_list","nome",$sql_where_site);
		$site_url_publicacao = $mysql->get_value("capture_site_list","url",$sql_where_site);
		$site_view_from_site_publicacao = $mysql->get_value("capture_site_list","view_from_site",$sql_where_site);
		//$site_view_from_site_publicacao = "S";

		$referencia_visualizar = " / " . strtolower($titulo_publicacao);
		$head_title = $titulo_publicacao . " - " . $head_title;
		
		$link_votacao = "http://www.telosonline.info/?url=" . $cripto->hide("http://www.telosonline.info/?p=visualizar&id=$id_publicacao");
		
		$page_views_publicacao++;
		$mysql->set_value("capture_post_site","page_views",$page_views_publicacao,$sql_where_publicacao);
		
		if ($site_view_from_site_publicacao == "S") {
			$content_type_publicacao = $mysql->get_value("capture_site_list","content_type",$sql_where_site);
			$site_pointer_start_publicacao = $mysql->get_value("capture_site_list","pointer_start",$sql_where_site);
			$site_pointer_stop_publicacao = $mysql->get_value("capture_site_list","pointer_stop",$sql_where_site);

			$fp = fopen($url_publicacao, 'r');
			$xml = "";
			while (!feof($fp)) {
				$xml .= fread($fp, 128);
			}
			fclose($fp);
			
			$xml = mb_convert_encoding($xml,"utf-8","$content_type_publicacao, auto");
			
			$temporario = explode($site_pointer_start_publicacao, $xml);
			$integra_publicacao = explode($site_pointer_stop_publicacao, $temporario[1]);

			$nota_destaque_publicacao++;
			$mysql->set_value("capture_post_site","nota_destaque",$nota_destaque_publicacao,$sql_where_publicacao);
		}
		
		$notatotal_destaque = $mysql->record_sum("capture_post_site","nota_destaque",$sql_where_publicacao);
		if ($nota_destaque_publicacao == 0) {
			$nota_destaque_publicacao = 0;
		} else {
			$nota_destaque_publicacao = ceil(($nota_destaque_publicacao*100)/$notatotal_destaque);
		}
	}
?>