<?php
/*
	Classe/Fun��o: conversor de data
	Modificado: 24/10/2007
	Autor: TelosOnline.info by Heron Santos
*/
	// conversor de data
	function convertedata($data, $separador, $formato) {
		switch($formato) {
			case "mysql.normal":
				$data = substr("$data", 8, 2) . $separador . substr("$data", 5, 2) . $separador . substr("$data", 0, 4); break;
			case "normal.mysql":
				$data = substr("$data", 6, 4) . $separador . substr("$data", 3, 2) . $separador . substr("$data", 0, 2); break;
			case "mysql.mysql":
				$data = substr("$data",0,4) . $separador . substr("$data", 5, 2) . $separador . substr("$data", 8, 2); break;
		}
		return $data;
	}
	
	// conversor para extenso
	function conversorextenso($valor, $formato) {
		// semana: numero para extenso
		if ($formato == "num.semana") {
			switch($valor) {
				case "1": $valor="domingo"; break;
				case "2": $valor="segunda-feira"; break;
				case "3": $valor="ter&ccedil;a-feira"; break;
				case "4": $valor="quarta-feira"; break;
				case "5": $valor="quinta-feira"; break;
				case "6": $valor="sexta-feira"; break;
				case "7": $valor="s&aacute;bado"; break;
			}
		}
		// mes: numero para extenso
		if ($formato == "num.mes") {
			switch($valor) {
				case "1": $valor="janeiro"; break;
				case "2": $valor="fevereiro"; break;
				case "3": $valor="mar&ccedil;o"; break;
				case "4": $valor="abril"; break;
				case "5": $valor="maio"; break;
				case "6": $valor="junho"; break;
				case "7": $valor="julho"; break;
				case "8": $valor="agosto"; break;
				case "9": $valor="setembro"; break;
				case "10": $valor="outubro"; break;
				case "11": $valor="novembro"; break;
				case "12": $valor="dezembro"; break;
			}
		}
		// semana curta: extenso para numero
		if ($formato == "semanacurta.num") {
			switch($valor) {
				case "Sun": $valor=1; break;
				case "Mon": $valor=2; break;
				case "Tue": $valor=3; break;
				case "Wed": $valor=4; break;
				case "Thu": $valor=5; break;
				case "Fri": $valor=6; break;
				case "Sat": $valor=7; break;
			}
		}
		// semana curta: semana extenso
		if ($formato == "semanacurta.semana") {
			switch($valor) {
				case "Sun": $valor="domingo"; break;
				case "Mon": $valor="segunda-Feira"; break;
				case "Tue": $valor="ter&ccedil;a-feira"; break;
				case "Wed": $valor="quarta-feira"; break;
				case "Thu": $valor="quinta-feira"; break;
				case "Fri": $valor="sexta-feira"; break;
				case "Sat": $valor="s&aacute;bado"; break;
			}
		}
		return $valor;
	}
?>