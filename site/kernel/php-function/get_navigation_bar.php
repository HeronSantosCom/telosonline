<?php 
/*
	Classe/Função: barra de navegacao
	Modificado: 04/02/2008
	Autor: TelosOnline.info by Heron Santos
*/
	$categorias = trim($_GET["categorias"]);
	$filtros = trim($_GET["filtros"]);
	if(empty($filtros)) { $filtros = trim($_POST["filtros"]); }
	$listagens = trim($_GET["listagens"]);	
	$id = trim($_GET["id"]);
	
	if (!empty($categorias)) { 
		$sql_where = "(id_site_list in(select site.id from capture_site_list site where (id_categoria_list in(select cat.id from capture_categoria_list cat where cat.id=$categorias))))";
		$link_categorias = "&amp;categorias=$categorias";
		$referencia_categorias = " / " . strtolower($mysql->get_value("capture_categoria_list","categoria","id = $categorias"));
	}
	
	if (!empty($filtros)) { 
		if (empty($categorias)) {
			$sql_where = "titulo like \"%".$filtros."%\" and descricao like \"%".$filtros."%\"";
		} else {
			$sql_where .= " and titulo like \"%".$filtros."%\" and descricao like \"%".$filtros."%\"";
		}
		$link_filtros = "&amp;filtros=$filtros";
		$referencia_filtros .= " / " . $filtros;
	}

	if (!empty($listagens)) { 
		switch ($listagens) {
			case "destaques":
				$sql_order = "nota_destaque desc,public_time desc,public_date desc,id desc";
			break;
			case "novidades":
				$sql_order = "public_date desc,public_time desc,id desc";
			break;
			case "populares":
				$sql_order = "page_views desc,public_time desc,public_date desc,id desc";
			break;
			default:
				$sql_order = "public_date desc,public_time desc,id desc";
				$listagens = "sem defini&ccedil;&atilde;o";
			break;
		}
		$link_listagens .= "&amp;listagens=$listagens";
		$referencia_listagens .= " / " . $listagens;
	} else {
		$sql_order = "public_date desc,public_time desc,id desc";
		$referencia_listagens .= " / geral";
	}
?>