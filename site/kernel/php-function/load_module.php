<?php 
/*
	Classe/Função: carregador de módulo
	Modificado: 24/10/2007
	Autor: TelosOnline.info by Heron Santos
*/
	//error_reporting(0);

	$module_name = $_GET["m"];
	$module_page = $_GET["p"];

	if (empty($module_name)) $module_name = "publicacoes";
	if (empty($module_page)) $module_page = "index";
	
	if (!is_dir("capture.module/$module_name")) {
		$module_name = "erro";
		$module_page = "module.page";
	} elseif (!fopen("capture.module/$module_name/$module_page.php","r")) {
		$module_name = "erro";
		$module_page = "module.page";
	}
	$opener = "capture.module/$module_name/$module_page.php";
?>