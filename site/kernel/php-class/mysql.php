<?php
/*
	Classe/Função: Realiza conexão com o banco de dados
	Modificado: 13/02/2008
	Autor: Heron Santos by TelosOnline.info
*/
/*
	$mysql = new MySQL;
	$mysql->get_sql("SELECT * FROM amigos");
	$mysql->next_registry("codigo","compras");
*/
/* verificador de seguança */
$checkurl = $_SERVER["PHP_SELF"];
if (eregi("mysql.php", "$checkurl")) {
	header ("Location: ../../index.php");
}
class MySQL {
	/* variaveis */
	var $hostname = "localhost"; // endereço do banco de dados
	var $port     = "3306"; // porta do banco de dados
	//var $username = "75764"; // usuário do banco de dados
	var $username = "root"; // usuário do banco de dados
	//var $password = "b9295993"; // senha do banco de dados
	var $password = ""; // senha do banco de dados
	var $name     = "75764"; // nome do banco de dados
    
	/* construtor */
	public function __construct() {
        
    }
	
	/* conecta ao banco de dados */
	public function connect() {
		$this->connection = mysql_connect("$this->hostname:$this->port",$this->username,$this->password);
		if (!$this->connection) {
			echo "Erro na Conex&atilde;o.<br>"."<b>MySQL retornou: </b> ".mysql_error()."<br>";
			die();
		} elseif (!mysql_select_db($this->name,$this->connection)) {
			echo "Erro na sele&ccedil;&atilde;o do Banco de Dados.<br>"."<b>MySQL retornou: </b> ".mysql_error()."<br>";
			die();
		}
    }
	
	/* desconecta do banco de dados */
	public function disconnect() {
		return mysql_close($this->connection);
	}
	
	/* executa query sql */
	public function get_sql($value) {
		$this->connect();
		$this->pvalue = trim($value);
		if ($this->result = mysql_query($this->pvalue)) {
			return $this->result;
			$this->disconnect();
		} else {
			die("Ocorreu um erro ao executar esta operacao!<br><br>"."<b>MySQL Retornou: <b> ".mysql_error()."<br>");
			$this->disconnect();
		}
	}
	
	/* executa query sql e retorna o valor da coluna */
	public function get_value($tabela,$coluna,$condicao) {
		$this->connect();
		$this->ptabela = trim($tabela);
		$this->pcoluna = trim($coluna);
		$this->pcondicao = trim($condicao);
		
		if (!empty($this->pcondicao)) $this->pcondicao = " where ".$this->pcondicao;
		
		if ($this->select = mysql_query("select * from ".$this->ptabela.$this->pcondicao)) {
			$this->pvalue = mysql_result($this->select, 0, $this->pcoluna);
			return $this->pvalue;
			$this->disconnect();
		} else {
			die("Ocorreu um erro ao obter valor no banco de dados!<br><br>"."<b>MySQL Retornou: <b> ".mysql_error()."<br>");
			$this->disconnect();
		}
	}
	
	/* executa query sql e grava o valor da coluna */
	public function set_value($tabela,$coluna,$valor,$condicao) {
		$this->connect();
		$this->ptabela = trim($tabela);
		$this->pcoluna = trim($coluna);
		$this->pvalor = trim($valor);
		$this->pcondicao = trim($condicao);
		
		if (!empty($this->pcondicao)) $this->pcondicao = " where ".$this->pcondicao;

		if ($this->update = mysql_query("update ".$this->ptabela." set ".$this->pcoluna." = ".$this->pvalor.$this->pcondicao)) {
			$this->disconnect();
		} else {
			die("Ocorreu um erro ao atualizar valor no banco de dados!<br><br>"."<b>MySQL Retornou: <b> ".mysql_error()."<br>");
			$this->disconnect();
		}
	}
	
	/* encontra o próximo registro */
	public function next_registry($tabela,$coluna) {
		$this->connect();
		$this->pcoluna = trim($coluna);
		$this->ptabela = trim($tabela);
		
		$this->rows = mysql_num_rows(mysql_query("select * from ".$this->ptabela));
		if ($this->rows > 0) { // se existe algum registro
			$this->maxrows = mysql_query("select max(".$this->pcoluna.")+1 as total from ".$this->ptabela." where 1");
			while (is_array($record = mysql_fetch_array($this->maxrows))) {
				$this->pvalue = $record["total"];
			}
		} else {
			$this->pvalue = 1;
		}
		return $this->pvalue;
		$this->disconnect();
	}
	
	/* retorna o total de registros */
	public function record_count($tabela,$condicao) {
		$this->connect();
		$this->ptabela = trim($tabela);
		$this->pcondicao = trim($condicao);
		
		if (!empty($this->pcondicao)) $this->pcondicao = " where ".$this->pcondicao;

		$this->count = mysql_query("SELECT COUNT(*) AS total FROM ".$this->ptabela.$this->pcondicao);
		$this->pvalue = mysql_result($this->count, 0, "total");

		return $this->pvalue;
		$this->disconnect();
	}
	
	/* retorna o acumulado de registros */
	public function record_sum($tabela,$coluna,$condicao) {
		$this->connect();
		$this->ptabela = trim($tabela);
		$this->pcoluna = trim($coluna);
		$this->pcondicao = trim($condicao);

		$this->sum = mysql_query("SELECT SUM(".$this->pcoluna.") AS total FROM ".$this->ptabela." where ".$this->pcondicao."");
		$this->pvalue = mysql_result($this->sum, 0, "total");

		return $this->pvalue;
		$this->disconnect();
	}
}
?>