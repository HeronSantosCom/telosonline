<?php
	header("Cache-Control: no-cache, must-revalidate");
	header("Pragma: no-cache");
	session_start();
	
	$system_name    = "TelosOnline.info"; // nome do sistema
	$system_sign    = "TOL.info"; // sigla do sistema
	$system_version = "13"; // vers�o do sistema
	
	$head_title = $system_name. " v" .$system_version;
	$head_description = "Portal de conte�do tecnol�gico voltado para inform�tica com artigos, not�cias e mais de 30 cursos entre ele inform�tica, programa��o, anima��o, designer, profissionalizante, l�ngua estrangeira...";
	$head_copyright = "shopping cart, asp, php, linux, web hosting, curso de, sql hosting, script, asp net, php script, hosting, php tutorial, tecnologia";
	$head_webmaster = "heronrsantos@gmail.com";
 
	$patch_root   = "/projetos/telosonline.info"; // pasta do projeto
	$patch_kernel = $patch_root."/kernel"; // pasta principal do projeto

	require("kernel/php-class/mysql.php");
	$mysql = new MySQL;

	require("kernel/php-class/cryptographic.php");
	$cripto = new CripTo;
?>