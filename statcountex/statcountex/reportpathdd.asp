<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%><%pageProtege = True%><!--#include file="config.asp"--><html><head><title>StatCounteX 3.2</title><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><link href="style.css" rel="stylesheet" type="text/css"><script language="JavaScript" type="text/JavaScript"><!--
function MM_goToURL() { //v3.0
var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//--></script></head><body><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td class="grande"><strong>StatCounteX 3.2 - Relat&oacute;rio</strong></td><td align="right" class="pequeno"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/reportbug.asp?pid=4" target="_blank">Reportar um Erro</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/recommend.asp?pid=4" target="_blank">Recomende este projeto </a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/question.asp?pid=4" target="_blank">FAQ</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/submitsite.asp?pid=4" target="_blank">Inscrever um site</a></td></tr><tr><td colspan="2"><hr width="100%" color="#336699" size="1" noshade></td></tr></table><%
	'-------------------------------------------------------------
	'StatCounteX 3.2
	'http://www.2enetworx.com/dev/projects/statcountex.asp
	
	'File: reportpathdd.asp
	'Description: Daily Report
	'Initiated by Hakan Eskici on Nov 18, 2000

	'See credits.txt for the list of contributors

	'You may use the code for any purpose
	'But re-publishing is discouraged.
	'See License.txt for additional information	

	'-------------------------------------------------------------
	'Change Log:
	'-------------------------------------------------------------
	'# Rami Kattan, http://www.kattanweb.com/webdev
	'Support for reporting WinME and Netscape 6.x
	'"All" link to Top10 referers and views
	'# Feb 5, 2001 by Kevin Yochum
	'Added links for graphical reports and visitor ip paths
	'Added showing url's as active links
	'-------------------------------------------------------------
%><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5" class="caixacinza"><tr><td class="pequeno"><a href="reports.asp">Relat&oacute;rio</a> » <a href="reportpathy.asp">Relat&oacute;rio Anual</a> » <a href="reportpathm.asp?year=<%=request("year")%>">Relat&oacute;rio Mensal</a> » <a href="reportpathd.asp?year=<%=request("year")%>&month=<%=request("month")%>">Relat&oacute;rio Di&aacute;rio</a><br><br><%	sub SQLTable(sTitle, sSQL)
		rs.Open sSQL, , , adCmdTable
		if err <> 0 then response.end %><br><strong class="grande">&raquo;&nbsp;<%=sTitle%><br><br></strong><table border=0 cellpadding=0 cellspacing=5 class="caixabranca"><tr><%		for each fld in rs.fields
			iFieldCount = iFieldCount + 1
			if iFieldCount = 1 then
				sAlign="left"
			else
				sAlign="right"
			end if
			%><td width="150" align="<%=sAlign%>" class="pequeno"><%=fld.name%></td><%		next %></tr><%		do while not rs.eof %><tr><%			iFieldCount = 0
			for each fld in rs.fields
				iFieldCount = iFieldCount + 1
				if iFieldCount = 1 then
					sAlign = "left"
				else
					sAlign="right"
				end if
				sFieldName = ""
				If Len(Trim(rs(fld.name))) > 100 Then
					fldName = Mid(rs(fld.name),1,100) & "..."
				Else
					fldName = rs(fld.name)
				End If
				If bShowLinks And InStr( rs(fld.name), "http://" ) > 0 Then
					sFieldName = "<a href='" & rs(fld.name) & "'>" & fldName & "</a>"
				Else
					sFieldName = fldName
				End if %><td align="<%=sAlign%>" class="pequeno"><%=sFieldName%></td><%			next %></tr><%		rs.movenext
		loop %></table><%		rs.close
	end sub
	OpenDB sConnStats
	sYear = request("year")
	sMonth = request("month")
	sDay = request("day")
	sDate = "#" & sMonth & "/" & sDay & "/" & sYear & "#"
	response.write "Estatística por " & MonthName(sMonth) & " " & sDay & ", " & sYear & "<br>"
	qstring = request.servervariables("query_string")
	if request.querystring("rall") <> "" or request.querystring("pall") <> "" then
		qstring = left (qstring, instrrev(qstring, "&") - 1)
	end if
	if request.querystring("pall") <> "yes" then pTop10 = "TOP 10"
		SQLTable "<a href=""reportpathdd.asp?" & qstring & """>Top 10 Pages</a> - <a href=""reportpathdd.asp?" & qstring & "&pall=yes"">All Pages</a>", "SELECT " & pTop10 & " PathName, COUNT(PathName) AS Total FROM Stats LEFT JOIN Paths ON (Stats.PathID=Paths.PathID) WHERE Date = " & sDate & " GROUP BY PathName ORDER BY COUNT(PathName) DESC"
	if request.querystring("rall") <> "yes" then rTop10 = "TOP 10"
		SQLTable "<a href=""reportpathdd.asp?" & qstring & """>Top 10 Referers</a> - <a href=""reportpathdd.asp?" & qstring & "&rall=yes"">All Referers</a>", "SELECT " & rTop10 & " RefName, COUNT(RefName) AS Total FROM Stats LEFT JOIN Refs ON (Stats.RefID=Refs.RefID) WHERE Date = " & sDate & " GROUP BY RefName ORDER BY COUNT(RefName) DESC"
		SQLTable "Browsers", "SELECT BrowserName, COUNT(BrowserName) AS Total FROM Stats LEFT JOIN Browsers ON (Stats.BrowserID=Browsers.BrowserID) WHERE Date = " & sDate & " GROUP BY BrowserName ORDER BY COUNT(BrowserName) DESC"
		SQLTable "Resolutions", "SELECT ResName, COUNT(ResName) AS Total FROM Stats LEFT JOIN Resolutions ON (Stats.ResID=Resolutions.ResID) WHERE Date = " & sDate & " GROUP BY ResName ORDER BY COUNT(ResName) DESC"
		SQLTable "Colors", "SELECT ColorName, COUNT(ColorName) AS Total FROM Stats LEFT JOIN Colors ON (Stats.ColorID=Colors.ColorID) WHERE Date = " & sDate & " GROUP BY ColorName ORDER BY COUNT(ColorName) DESC"
		'SQLTable "Operating Systems", "SELECT OSName, COUNT(OSName) AS Total FROM Stats LEFT JOIN OSes ON (Stats.OsID=OSes.OsID) WHERE Date = " & sDate & " GROUP BY OsName ORDER BY COUNT(OsName) DESC"
		conn.close
		set rs=nothing
		set conn=nothing %><br><strong class="grande">Estat&iacute;stica Detalhada</strong><br>
»


Ver <a href="ips.asp?Year=<%=sYear%>&Month=<%=sMonth%>&Day=<%=sDay%>">cliques paths (Endere&ccedil;o IP)</a>.<br><br><strong class="grande">Gr&aacute;ficos (<%=MonthName(sMonth) & " " & sDay & ", " & sYear%>)</strong><br>» 
























Ver <a href="graphs.asp?Type=Hour&Year=<%=sYear%>&Month=<%=sMonth%>&Day=<%=sDay%>">Page Views e Visitas por Hora</a>.</td></tr></table><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr align="center"><td colspan="2" class="pequeno"><table border="0" cellspacing="0" cellpadding="0"><tr><td><input name="back" type="submit" class="textoboxcinza" id="back" onClick="MM_goToURL('parent','reports.asp');return document.MM_returnValue" value="Voltar para Pagina Principal"></td><td width="5">&nbsp;</td><td><input name="back2" type="submit" class="textoboxcinza" id="back22" onClick="MM_goToURL('parent','admin.asp');return document.MM_returnValue" value="P&aacute;gina de Administra&ccedil;&atilde;o"></td><td width="5">&nbsp;</td><td><input name="back2" type="submit" class="textoboxcinza" id="back" onClick="MM_goToURL('parent','default.asp?acao=logoff');return document.MM_returnValue" value="   Efetuar Logoff...   "></td></tr></table></td></tr><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr><td class="pequeno">Visite <a href="../redirect.asp?http://www.2enetworx.com/dev">2eNetWorX</a> para saber mais sobre OpenSource VB e ASP Projects. Traduzido por <a href="http://www.telosonline.rg.com.br/">Telos Online</a>.</td><td align="right"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/statcountex.asp"><img src="icon.statcountex.gif" width=80 height=15 alt="StatCounteX" border="0"></a></td></tr></table></body></html>