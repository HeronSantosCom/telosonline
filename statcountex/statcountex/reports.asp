<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%><%pageProtege = True%><!--#include file="config.asp"--><html><head><title>StatCounteX 3.2</title><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><link href="style.css" rel="stylesheet" type="text/css"><script language="JavaScript" type="text/JavaScript"><!--
function MM_goToURL() { //v3.0
var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//--></script></head><body><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td class="grande"><strong>StatCounteX 3.2 - Relat&oacute;rio</strong></td><td align="right" class="pequeno"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/reportbug.asp?pid=4" target="_blank">Reportar um Erro</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/recommend.asp?pid=4" target="_blank">Recomende este projeto </a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/question.asp?pid=4" target="_blank">FAQ</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/submitsite.asp?pid=4" target="_blank">Inscrever um site</a></td></tr><tr><td colspan="2"><hr width="100%" color="#336699" size="1" noshade></td></tr></table><%
'-------------------------------------------------------------
'StatCounteX 3.2
'http://www.2enetworx.com/dev/projects/statcountex.asp

'File: admin.asp
'Description: StatCounteX Reports Main Page
'Initiated by Hakan Eskici on Nov 18, 2000

'See credits.txt for the list of contributors

'You may use the code for any purpose
'But re-publishing is discouraged.

'-------------------------------------------------------------
'Change Log:
'-------------------------------------------------------------
%><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5" class="caixacinza"><tr><td class="pequeno"><%

sToday = month(date) & "/" & day(date) & "/" & year(date)
'Fixed by Don
dtYesterday =DateAdd("d", -1, now)
sYesterday = month(dtYesterday) & "/" & day(dtYesterday) & "/" & year(dtYesterday)

OpenDB sConnStats

'Total PageViews
sSQL = "SELECT COUNT(StatID) AS Total FROM Stats"
rs.Open sSQL,,,adCmdTable
lPageViewsTotal = rs("Total")
rs.close

sSQL = "SELECT COUNT(StatID) AS Total FROM Stats WHERE Date = #" & sToday & "#"
rs.Open sSQL,,,adCmdTable
lPageViewsToday = rs("Total")
rs.close

sSQL = "SELECT COUNT(StatID) AS Total FROM Stats WHERE Date = #" & sYesterday & "#"
rs.Open sSQL,,,adCmdTable
lPageViewsYesterday = rs("Total")
rs.close

sSQL = "SELECT IP FROM Stats GROUP BY IP"
rs.Open sSQL,,,adCmdTable
lVisitorsTotal = rs.recordcount
rs.close

sSQL = "SELECT IP FROM Stats WHERE Date = #" & sToday & "# GROUP BY IP"
rs.Open sSQL,,,adCmdTable
lVisitorsToday = rs.recordcount
rs.close

sSQL = "SELECT IP FROM Stats WHERE Date = #" & sYesterday & "# GROUP BY IP"
rs.Open sSQL,,,adCmdTable
lVisitorsYesterday = rs.recordcount
rs.close

sSQL = "SELECT TOP 1 * FROM TopPageViewsPerDay"
rs.Open sSQL,,,adCmdTable
 If Not Rs.eof Then
 rs.MoveFirst
 lTopViews = rs( "Total" )
 sTopViewsDay = rs("Date")
End If
rs.close

sSQL = "SELECT TOP 1 * FROM TopIpsPerDay"
rs.Open sSQL,,,adCmdTable
If Not Rs.Eof Then
 rs.MoveFirst
 lTopVisitors = rs( "Total" )
 sTopVisitorsDay = rs("Date")
End If
rs.close

sPageViewsToday = formatnumber(lPageViewsToday, 0)
sPageViewsYesterday = formatnumber(lPageViewsYesterday, 0)
sPageViewsTotal = formatnumber(lPageViewsTotal, 0)

sVisitorsToday = formatnumber(lVisitorsToday, 0)
sVisitorsYesterday = formatnumber(lVisitorsYesterday, 0)
sVisitorsTotal = formatnumber(lVisitorsTotal, 0)

 sTopViews = formatnumber( lTopViews, 0 )
 sTopVisitors = formatnumber( lTopVisitors, 0 )
%><span class="grande"><strong>Sum&aacute;rio<br><br></strong></span><table border="0" align="center" cellpadding="0" cellspacing="5"><tr align="center" valign="top" class="pequeno"><td><table width=300 border=0 align="center" cellpadding="0" cellspacing=5 class="caixabranca"><tr class="pequeno"><td></td><td align=center><strong>PageViews</strong></td><td align=center><strong>Visitantes</strong></td></tr><tr class="pequeno"><td align="right"><strong><a href="reportpathdd.asp?year=<%=Year(Now())%>&month=<%=Month(Now())%>&day=<%=Day(Now())%>">Hoje</a>:</strong></td><td align=center><%=sPageViewsToday%></td><td align=center><%=sVisitorsToday%></td></tr><tr class="pequeno"><%
'Fixed by Don
%><td align="right"><strong><a href="reportpathdd.asp?year=<%=Year(dtYesterday)%>&month=<%=Month(dtYesterday)%>&day=<%=Day(dtYesterday)%>">Ontem</a>:</strong></td><td align=center><%=sPageViewsYesterday%></td><td align=center><%=sVisitorsYesterday%></td></tr><tr class="pequeno"><td align="right"><strong>Top Dia:</strong></td><td align=center><a href="reportpathdd.asp?year=<%=Year(sTopViewsDay)%>&month=<%=Month(sTopViewsDay)%>&day=<%=Day(sTopViewsDay)%>"><%=sTopViews%><br>
            (<%=sTopViewsDay%>)</a></td><td align=center><a href="reportpathdd.asp?year=<%=Year(sTopVisitorsDay)%>&month=<%=Month(sTopVisitorsDay)%>&day=<%=Day(sTopVisitorsDay)%>"><%=sTopVisitors%><br>
            (<%=sTopVisitorsDay%>)</a></td></tr><tr class="pequeno"><td align="right"><strong>Total:</strong></td><td align=center><%=sPageViewsTotal%></td><td align=center><%=sVisitorsTotal%></td></tr></table></td><td><!--#include file="calendar.asp"--></td></tr></table><span class="grande"><strong>Estat&iacute;stica Detalhada (Todos os Dados)
</strong></span><br>
� 
Ver <a href="reportd.asp">relat&oacute;rio detalhado contendo estatistica de navegadores, resolu&ccedil;&atilde;o, cores, sistemas operacionais,
p&aacute;gina and refer&ecirc;ncias</a>.<br>
� 
Ver <a href="reportpath.asp">page views de todas as p&aacute;ginas</a>.<br>� 





















Ver <a href="reportref.asp">todas as refer&ecirc;ncias</a>.<br>� 































Ver <a href="reportpathy.asp">hor&aacute;rio espec&iacute;fico de page views</a>.<br>� 






















Ver <a href="ips.asp">cliques paths (Endere&ccedil;o IP)</a>.<br><br><span class="grande"><strong>Gr&aacute;ficos (Todos os Dados)</strong></span><br>
� 

Ver <a href="graphs.asp?Type=Hour">page views e visitantes por hora</a>.<br>
� 






















Ver <a href="graphs.asp?Type=DOW">page views e visitantes por dia/semana</a>.<br>
� 







Ver <a href="graphs.asp?Type=DOM">page views e visitantes por dia/m&ecirc;s</a>.<br>
� 










Ver <a href="graphs.asp?Type=Week">page views e visitantes por semana/ano</a>.<br>
� 



Ver <a href="graphs.asp?Type=Month">page views e visitantes por m&ecirc;s</a>.<br>
� 



Ver <a href="graphs.asp?Type=Year">page views e visitantes por ano</a>.</td></tr></table><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr align="center"><td colspan="2" class="pequeno"><table border="0" cellspacing="0" cellpadding="0"><tr><td><input name="back2" type="submit" class="textoboxcinza" id="back22" onClick="MM_goToURL('parent','admin.asp');return document.MM_returnValue" value="P&aacute;gina de Administra&ccedil;&atilde;o"></td><td width="5">&nbsp;</td><td><input name="back2" type="submit" class="textoboxcinza" id="back" onClick="MM_goToURL('parent','default.asp?acao=logoff');return document.MM_returnValue" value="   Efetuar Logoff...   "></td></tr></table></td></tr><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr><td class="pequeno">Visite <a href="../redirect.asp?http://www.2enetworx.com/dev">2eNetWorX</a> para saber mais sobre OpenSource VB e ASP Projects. Traduzido por <a href="http://www.telosonline.rg.com.br/">Telos Online</a>.</td><td align="right"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/statcountex.asp"><img src="icon.statcountex.gif" width=80 height=15 alt="StatCounteX" border="0"></a></td></tr></table></body></html>