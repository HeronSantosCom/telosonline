<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%><%pageProtege = True%><!--#include file="config.asp"--><html><head><title>StatCounteX 3.2</title><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><link href="style.css" rel="stylesheet" type="text/css"><script language="JavaScript" type="text/JavaScript"><!--
function MM_goToURL() { //v3.0
var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//--></script></head><body><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td class="grande"><strong>StatCounteX 3.2 - Administra&ccedil;&atilde;o</strong></td><td align="right" class="pequeno"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/reportbug.asp?pid=4" target="_blank">Reportar um Erro</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/recommend.asp?pid=4" target="_blank">Recomende este projeto </a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/question.asp?pid=4" target="_blank">FAQ</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/submitsite.asp?pid=4" target="_blank">Inscrever um site</a></td></tr><tr><td colspan="2"><hr width="100%" color="#336699" size="1" noshade></td></tr></table><table border="0" align="center" cellpadding="0" cellspacing="5" class="caixacinza"><form action="admin.pross.asp" method="post"><tr class="pequeno"><td colspan="2" valign="top"><table border="0" cellspacing="0" cellpadding="0"><tr><td class="pequeno"><strong>Local da Imagem:&nbsp;</strong></td><td><input name="ImageLocation" type="text" class="textobox" value="<%=sImageLocation%>" size="50"></td></tr></table>
sImageLocation &eacute; a contante que define o local da imagem usada pelo StatCounteX. (80x15)<br>
Ex.: ../img/statcountex.gif</td></tr><tr class="pequeno"><td colspan="2" valign="top"><table border="0" cellspacing="0" cellpadding="0"><tr><td class="pequeno"><strong>Filtro de IP's:&nbsp;</strong></td><td><input name="FilterIPs" type="text" class="textobox" value="<%=sFilterIps%>" size="75"></td></tr></table>
sFilterIps &eacute; a constante que define os endere&ccedil;os de IP que ser&atilde;o filtrados pelo relat&oacute;rio do StatCounteX.<br>
This is useful for webmasters that do not want their own activity reported in reports. List IPs separated by commas!</td></tr><tr class="pequeno"><td colspan="2" valign="top"><table border="0" cellspacing="0" cellpadding="0"><tr><td class="pequeno"><strong>Show links in reports as active links:&nbsp;</strong></td><td><select name="showlinks" class="textobox"><option value="1">Ligado
<option value="0">Desligado
</select></td></tr></table>If true, if a link is shown in a report, it can be clicked and the browser will display the referring page or viewed page.</td></tr><tr class="pequeno"><td colspan="2" valign="top"><table border="0" cellspacing="0" cellpadding="0"><tr><td class="pequeno"><strong>Count your own server as referer:&nbsp;</strong></td><td><select name="CountServer" class="textobox"><option value="1">Ligado
<option value="0">Desligado
</select></td></tr></table>If true, the click generated from your own site will be counted as referers as well.</td></tr><tr class="pequeno"><td colspan="2" valign="top"><table border="0" cellspacing="0" cellpadding="0"><tr><td class="pequeno"><strong>Ocultar Par&acirc;metros de P&aacute;ginas Internas:&nbsp;</strong></td><td><select name="PathParameters" class="textobox"><option value="1">Ligado
<option value="0">Desligado
</select></td></tr></table>
    Ligado:<em><font color="#FF0000"> http://www.2enetworx.com/index.asp?id=2&amp;cid=4</font></em><br>
    Desligado:<em><font color="#FF0000"> http://www.2enetworx.com/index.asp</font></em></td></tr><tr class="pequeno"><td colspan="2" valign="top"><table border="0" cellspacing="0" cellpadding="0"><tr><td class="pequeno"><strong>Ocultar Protocolo de P&aacute;ginas Internas:&nbsp;</strong></td><td><select name="PathProtocol" class="textobox"><option value="1">Ligado
<option value="0">Desligado
</select></td></tr></table>
    Ligado:<em><font color="#FF0000"> http://www.2enetworx.com/index.asp?id=2&amp;cid4</font></em><br>
    Desligado:<em><font color="#FF0000"> 2enetworx.com/index.asp?id=2&amp;cid4</font></em></td></tr><tr class="pequeno"><td colspan="2" valign="top"><table border="0" cellspacing="0" cellpadding="0"><tr><td class="pequeno"><strong>Ocultar Par&acirc;metro de Refer&ecirc;ncias:&nbsp;</strong></td><td><select name="RefParameters" class="textobox"><option value="1">Ligado
<option value="0">Desligado
</select></td></tr></table>
    Ligado: <em><font color="#FF0000">http://www.microsoft.com/go.asp?linkid=6</font></em><br>
    Desligado: <em><font color="#FF0000">http://www.microsoft.com/go.asp</font></em></td></tr><tr class="pequeno"><td colspan="2" valign="top"><table border="0" cellspacing="0" cellpadding="0"><tr><td class="pequeno"><strong>Ocultar Protocolo de Refer&ecirc;ncias:&nbsp;</strong></td><td><select name="RefProtocol" class="textobox"><option value="1">Ligado
<option value="0">Desligado
</select></td></tr></table>
    Ligado: <em><font color="#FF0000">http://www.microsoft.com/go.asp?linkid=6</font></em><br>
    Desligado: <em><font color="#FF0000">microsoft.com/go.asp?linkid=6</font></em></td></tr><tr class="pequeno"><td colspan="2" valign="top"><table border="0" cellspacing="0" cellpadding="0"><tr><td class="pequeno"><strong>Ocultar P&aacute;gina & Par&acirc;metros de Refer&ecirc;ncias:&nbsp;</strong></td><td><select name="RefFile" class="textobox"><option value="1">Ligado
<option value="0">Desligado
</select></td></tr></table>
Ligado:<em><font color="#FF0000"> http://www.microsoft.com/go.asp?linkid=6</font></em><br>
Desligado:<em><font color="#FF0000"> http://www.microsoft.com</font></em></td></tr><tr class="pequeno"><td align="right"><input type="hidden" value="change" name="method" id="hidden1"><input name="submit" type="submit" class="textobox" id="submit" value="Alterar Configura&ccedil;&otilde;es"></td><td><input name="reset" type="reset" class="textobox" id="reset" value="Cancelar Altera&ccedil;&atilde;o"></td></tr></form></table><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr align="center"><td colspan="2" class="pequeno"><table border="0" cellspacing="0" cellpadding="0"><tr><td><input name="back" type="submit" class="textoboxcinza" id="back" onClick="MM_goToURL('parent','codigocontador.asp');return document.MM_returnValue" value="C&oacute;digo do Contador!"></td></tr></table></td></tr>
  <tr align="center">
    <td colspan="2" class="pequeno"><hr width="100%" color="#336699" size="1" noshade></td>
  </tr>
  <tr align="center">
    <td colspan="2" class="pequeno"><table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><input name="back2" type="submit" class="textoboxcinza" id="back2" onClick="MM_goToURL('parent','reports.asp');return document.MM_returnValue" value="Voltar para Pagina Principal">
        </td>
        <td width="5">&nbsp;</td>
        <td><input name="back2" type="submit" class="textoboxcinza" id="back2" onClick="MM_goToURL('parent','default.asp?acao=logoff');return document.MM_returnValue" value="   Efetuar Logoff...   ">
        </td>
      </tr>
    </table></td>
  </tr>
  <tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr><td class="pequeno">Visite <a href="../redirect.asp?http://www.2enetworx.com/dev">2eNetWorX</a> para saber mais sobre OpenSource VB e ASP Projects. Traduzido por <a href="http://www.telosonline.rg.com.br/">Telos Online</a>.</td><td align="right"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/statcountex.asp"><img src="icon.statcountex.gif" width=80 height=15 alt="StatCounteX" border="0"></a></td></tr></table>
</body></html>
