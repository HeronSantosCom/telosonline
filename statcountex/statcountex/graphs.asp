<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%><%pageProtege = True%><!--#include file="config.asp"--><html><head><title>StatCounteX 3.2</title><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><link href="style.css" rel="stylesheet" type="text/css"><script language="JavaScript" type="text/JavaScript"><!--
function MM_goToURL() { //v3.0
var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//--></script></head><body><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td class="grande"><strong>StatCounteX 3.2 - Relat&oacute;rio</strong></td><td align="right" class="pequeno"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/reportbug.asp?pid=4" target="_blank">Reportar um Erro</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/recommend.asp?pid=4" target="_blank">Recomende este projeto </a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/question.asp?pid=4" target="_blank">FAQ</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/submitsite.asp?pid=4" target="_blank">Inscrever um site</a></td></tr><tr><td colspan="2"><hr width="100%" color="#336699" size="1" noshade></td></tr></table><%	'-------------------------------------------------------------
	'StatCounteX 3.2
	'http://www.2enetworx.com/dev/projects/statcountex.asp
	
	'File: graphs.asp
	'Description: Graphical Report Generator
	'Initiated by Kevin Yochum on Feb 5, 2001
	'-------------------------------------------------------------

	'You may use the code for any purpose
	'But re-publishing is discouraged.
	'See License.txt for additional information	

	'-------------------------------------------------------------
	'Change Log:
	'-------------------------------------------------------------
	'# Feb 5, 2001 by Hakan Eskici
	'Added WeekDayName to GraphDayOfWeek subroutine
	'Added division by zero check for calculations
	'-------------------------------------------------------------

	' Sub GraphHours
	'
	' Usage:
	' sViewType - The type of content to display. Values: "Visits" or "Views".
	' lYear - a four-digit year or "" for none.
	' lMonth - a 0-based one- or two-digit month number.
	' lDay - a 1-based one- or two- digit day number.
	'
	Sub GraphHours( sViewType, lYear, lMonth, lDay ) 
		If sViewType = "Views" Then
			sSQL = "SELECT Format( Stats.Time, 'h') as [Hour], Count(Stats.IP) AS Total "
			sSQL = sSQL & "FROM Stats "
			sGroupBy = "GROUP BY Format( Stats.Time, 'h') "
			sDataSource = "Stats"
		ElseIf sViewType = "Visits" Then
			sSQL = "SELECT GroupIpsByHourAndDate.Hour, Count(GroupIpsByHourAndDate.IP) AS Total "
			sSQL = sSQL & "FROM GroupIpsByHourAndDate "
			sGroupBy = "GROUP BY GroupIpsByHourAndDate.Hour "
			sDataSource = "GroupIpsByHourAndDate"
		Else
			Exit Sub
		End If
		sHaving = sHaving & "HAVING ((1=1) "
		If Len( lYear ) > 0 Then
			sHaving = sHaving & "AND (Format( "& sDataSource &".Date, 'yyyy')=" & lYear & ") "
			sGroupBy = sGroupBy & ", Format( "& sDataSource &".Date, 'yyyy') "
		End If
		If Len( lMonth ) > 0 Then
			sHaving = sHaving & "AND (Format( "& sDataSource &".Date, 'm')=" & lMonth & ") "
			sGroupBy = sGroupBy & ", Format( "& sDataSource &".Date, 'm') "
		End If
		If Len( lDay ) > 0 Then
			sHaving = sHaving & "AND (Format( "& sDataSource &".Date, 'd')=" & lDay & ") "
			sGroupBy = sGroupBy & ", Format( "& sDataSource &".Date, 'd') "
		End If 
		sHaving = sHaving & ")"
		sSQL = sSQL & sGroupBy & sHaving
		' Connect to the database
		OpenDB sConnStats
		rs.Open sSQL,,,adCmdTable
		Dim aHours(23)
		lTotal = 0
		lTop = 0
		' Calculate totals
		do while not rs.eof
			If rs("Total") > lTop Then
				lTop = rs("Total")
			End If
			lTotal = lTotal + rs("Total")
			aHours( rs("Hour") ) = rs("Total")
		rs.movenext
		loop	
		' Display results %><strong class="grande"><br>
&raquo; Estat&iacute;stica: <%=sViewType%> por hora</strong><br><br><table border="0" cellpadding="0" cellspacing="5" class="caixabranca"><tr class="pequeno"><td valign="bottom">&nbsp;</td><%		For Each lCounter in aHours
			'Division by zero check
			'Added by HE, Feb 5, 2001
			if lTop > 0 then
				lHeight = (lCounter/lTop) * 150
			else
				lHeight = 0
			end if %><td height="150" align="center" valign="bottom"><img src="dr.gif" height=<%=lHeight%> width=10 alt="<%=lCounter%>"></td><%		Next %></tr><tr class="pequeno"><td align="right"><strong><%=sViewType%>:&nbsp;</strong></td><% 	For Each lCounter in aHours %><td align="center"><%=FormatNumber(lCounter,0)%></td><% 	Next %></tr><tr class="pequeno"><td align="right"><strong>%:&nbsp;</strong></td><%		For Each lCounter in aHours
			'Division by zero check
			'Added by HE, Feb 5, 2001
			if lTotal > 0 then
				sPercent = FormatPercent(lCounter/lTotal,2)
			else
				sPercent = ""
			end if %><td align="center"><%=sPercent%></td><% 	Next %></tr><tr class="pequeno"><td align="right"><strong>Hora:&nbsp;</strong></td><% 	For lCounter = 0 to 23 %><td align="center"><%=lCounter%>:00</td><% 	Next %></tr></table><% 	conn.close
		'set rs=nothing
		'set conn=nothing
	End Sub
	'
	' Sub GraphDayOfWeek
	'
	' Usage:
	' sViewType - The type of content to display. Values: "Visits" or "Views".
	' lYear - a four-digit year or "" for none.
	' lMonth - a 0-based one- or two-digit month number.
	' lWeek - a 0-based one- or two-digit week (of year) number
	'
	Sub GraphDayOfWeek( sViewType, lYear, lMonth, lWeek )
		If sViewType = "Views" Then
			sSQL = "SELECT Format( Stats.Date, 'w') as [Day], Count(Stats.IP) AS Total "
			sSQL = sSQL & "FROM Stats "
			sGroupBy = "GROUP BY Format( Stats.Date, 'w') "
			sDataSource = "Stats"
		ElseIf sViewType = "Visits" Then
			sSQL = "SELECT Format( GroupIpsByDate.Date, 'w') as [Day], Count(GroupIpsByDate.IP) AS Total "
			sSQL = sSQL & "FROM GroupIpsByDate "
			sGroupBy = "GROUP BY Format( GroupIpsByDate.Date, 'w') "
			sDataSource = "GroupIpsByDate"
		Else
			Exit Sub
		End If
		sHaving = sHaving & "HAVING ((1=1) "
		If Len( lYear ) > 0 Then
			sHaving = sHaving & "AND (Format( "& sDataSource &".Date, 'yyyy')=" & lYear & ") "
			sGroupBy = sGroupBy & ", Format( "& sDataSource &".Date, 'yyyy') "
		End If
		If Len( lMonth ) > 0 Then
			sHaving = sHaving & "AND (Format( "& sDataSource &".Date, 'm')=" & lMonth & ") "
			sGroupBy = sGroupBy & ", Format( "& sDataSource &".Date, 'm') "
		End If
		If Len( lWeek ) > 0 Then
			sHaving = sHaving & "AND (Format( "& sDataSource &".Date, 'w')=" & lWeek & ") "
			sGroupBy = sGroupBy & ", Format( "& sDataSource &".Date, 'w') "
		End If 
		sHaving = sHaving & ")"
		sSQL = sSQL & sGroupBy & sHaving
		' Connect to the database
		OpenDB sConnStats
		rs.Open sSQL,,,adCmdTable
		Dim aDays(6)
		lTotal = 0
		lTop = 0
		'Calculate totals
		do while not rs.eof
			If rs("Total") > lTop Then
				lTop = rs("Total")
			End If
			lTotal = lTotal + rs("Total")
			aDays( rs("Day") -1) = rs("Total")
		rs.movenext
		loop	
		' Display results %><br><strong class="grande">&raquo; Estat&iacute;stica: <%=sViewType%> por dia da semana</strong><br><br><table border="0" cellpadding="0" cellspacing="5" class="caixabranca"><tr><td valign="bottom" class="pequeno"></td><% 	For Each lCounter in aDays
			'Division by zero check
			'Added by HE, Feb 5, 2001
			if lTop > 0 then
				lHeight = (lCounter/lTop ) * 150
			else
				lHeight = 0
			end if %><td height="150" align="center" valign="bottom" class="pequeno"><img src="dr.gif" height=<%=lHeight%> width=10 alt="<%=lCounter%>"></td><%		Next %></tr><tr><td align="right" class="pequeno"><strong><%=sViewType%>:&nbsp;</strong></td><%		For Each lCounter in aDays %><td align="center" class="pequeno"><%=FormatNumber(lCounter,0)%></td><%		Next %></tr><td align="right" class="pequeno"><strong>%:&nbsp;</strong></td><%		For Each lCounter in aDays %><td align="center" class="pequeno"><%=FormatPercent(lCounter/lTotal)%></td><%		Next %></tr><tr><td align="right" class="pequeno"><strong>Dia:&nbsp;</strong></td><%		For lCounter = 0 to 6 %><td align="center" class="pequeno"><%=WeekDayName(lCounter + 1) %></td><%		Next %></tr></table><% 	conn.close
		'set rs=nothing
		'set conn=nothing
	End Sub
	'
	' Sub GraphDayOfMonth
	'
	' Usage:
	' sViewType - The type of content to display. Values: "Visits" or "Views".
	' lYear - a four-digit year or "" for none.
	' lMonth - a 0-based one- or two-digit month number.
	'
	Sub GraphDayOfMonth( sViewType, lYear, lMonth, lWeek )
	If sViewType = "Views" Then
		sSQL = "SELECT Format( Stats.Date, 'd') as [Day], Count(Stats.IP) AS Total "
		sSQL = sSQL & "FROM Stats "
		sGroupBy = "GROUP BY Format( Stats.Date, 'd') "
		sDataSource = "Stats"
	ElseIf sViewType = "Visits" Then
		sSQL = "SELECT Format( GroupIpsByDate.Date, 'd') as [Day], Count(GroupIpsByDate.IP) AS Total "
		sSQL = sSQL & "FROM GroupIpsByDate "
		sGroupBy = "GROUP BY Format( GroupIpsByDate.Date, 'd') "
		sDataSource = "GroupIpsByDate"
	Else
		Exit Sub
	End If
	sHaving = sHaving & "HAVING ((1=1) "
	If Len( lYear ) > 0 Then
		sHaving = sHaving & "AND (Format( "& sDataSource &".Date, 'yyyy')=" & lYear & ") "
		sGroupBy = sGroupBy & ", Format( "& sDataSource &".Date, 'yyyy') "
	End If
	If Len( lMonth ) > 0 Then
		sHaving = sHaving & "AND (Format( "& sDataSource &".Date, 'm')=" & lMonth & ") "
		sGroupBy = sGroupBy & ", Format( "& sDataSource &".Date, 'm') "
	End If
	If Len( lWeek ) > 0 Then
		sHaving = sHaving & "AND (Format( "& sDataSource &".Date, 'w')=" & lWeek & ") "
		sGroupBy = sGroupBy & ", Format( "& sDataSource &".Date, 'w') "
	End If 
	sHaving = sHaving & ")"
	sSQL = sSQL & sGroupBy & sHaving
	' Connect to the database
	OpenDB sConnStats
	rs.Open sSQL,,,adCmdTable
	Dim aDays(30)
	lTotal = 0
	lTop = 0
	' Calculate totals
	do while not rs.eof
		If rs("Total") > lTop Then
			lTop = rs("Total")
		End If
		lTotal = lTotal + rs("Total") 
		aDays( rs("Day") -1) = rs("Total")
	rs.movenext
	loop	
	' Display results %><strong class="grande"><br>
&raquo; Estat&iacute;stica: <%=sViewType%> por dia do m&ecirc;s</strong><br><br><table border="0" cellpadding="0" cellspacing="5" class="caixabranca"><tr><td valign="bottom" class="pequeno"></td><%	For Each lCounter in aDays
		'Division by zero check
		'Added by HE, Feb 5, 2001
 		if lCounter > 0 then
			lHeight = (lCounter/lTop ) * 150
		else
			lHeight = 0
		end if %><td height="150" align="center" valign="bottom" class="pequeno"><img src="dr.gif" height=<%=lHeight%> width=10 alt="<%=lCounter%>"></td><%	Next %></tr><tr><td align="right" class="pequeno"><strong><%=sViewType%>:&nbsp;</strong></td><% For Each lCounter in aDays %><td align="center" class="pequeno"><%=FormatNumber(lCounter,0)%></td><%	Next %></tr><tr><td align="right" class="pequeno"><strong>%:&nbsp;</strong></td><%	For Each lCounter in aDays
		if lTotal > 0 then
			lPercent = lCounter / lTotal
		else
			lPercent = 0
		end if %><td align="center" class="pequeno"><%=FormatPercent(lPercent)%></td><%	Next %></tr><tr><td align="right" class="pequeno"><strong>Dia:&nbsp;</strong></td><%	For lCounter = 0 to 30
		sDay = ""
		If lYear > 0 and lMonth > 0 Then
			sDay = "<a href=""reportpathdd.asp?year=" & lYear & "&month=" & lMonth & "&day=" & lCounter + 1 & """>" & lCounter + 1 & "</a>"
		Else
			sDay = lCounter + 1
		End If %><td align="center" class="pequeno"><%=sDay%></td><%	Next %></tr></table><%		conn.close
		'set rs=nothing
		'set conn=nothing
	End Sub
	'
	' Sub GraphWeek
	'
	' Usage:
	' sViewType - The type of content to display. Values: "Visits" or "Views".
	' lYear - a four-digit year or "" for none.
	'
	Sub GraphWeek( sViewType, lYear )
		If sViewType = "Views" Then
			sSQL = "SELECT Format( Stats.Date, 'ww') as [Week], Count(Stats.IP) AS Total "
			sSQL = sSQL & "FROM Stats "
			sGroupBy = "GROUP BY Format( Stats.Date, 'ww') "
			sDataSource = "Stats"
		ElseIf sViewType = "Visits" Then
			sSQL = "SELECT Format( GroupIpsByDate.Date, 'ww') as [Week], Count(GroupIpsByDate.IP) AS Total "
			sSQL = sSQL & "FROM GroupIpsByDate "
			sGroupBy = "GROUP BY Format( GroupIpsByDate.Date, 'ww') "
			sDataSource = "GroupIpsByDate"
		Else
			Exit Sub
		End If
		sHaving = sHaving & "HAVING ((1=1) "
		If Len( lYear ) > 0 Then
			sHaving = sHaving & "AND (Format( "& sDataSource &".Date, 'yyyy')=" & lYear & ") "
			sGroupBy = sGroupBy & ", Format( "& sDataSource &".Date, 'yyyy') "
		End If
		sHaving = sHaving & ")"
		sSQL = sSQL & sGroupBy & sHaving
		' Connect to the database
		OpenDB sConnStats
		rs.Open sSQL,,,adCmdTable
		Dim aWeeks(53)
		lTotal = 0
		lTop = 0 
		' Calculate totals
		do while not rs.eof
			If rs("Total") > lTop Then
				lTop = rs("Total")
			End If
			aWeeks( rs("Week") -1) = rs("Total")
			lTotal = lTotal + rs("Total")
		rs.movenext
		loop	
		' Display results %><br><strong class="grande">&raquo; Estat&iacute;stica: <%=sViewType%> por semana do ano</strong><br><br><table border="0" cellpadding="0" cellspacing="5" class="caixabranca"><tr><td valign=bottom class="pequeno"></td><%		For Each lCounter in aWeeks
			'Division by zero check
			'Added by HE, Feb 5, 2001
			if lTop > 0 then
				lHeight = (lCounter/lTop ) * 150
			else
				lHeight = 0
			end if %><td height=150 align=center valign=bottom class="pequeno"><img src="dr.gif" height=<%=lHeight%> width=10 alt="<%=lCounter%>"></td><%		Next %></tr><tr><td align="right" class="pequeno"><strong><%=sViewType%>:&nbsp;</strong></td><%		For Each lCounter in aWeeks %><td align=center class="pequeno"><%=FormatNumber(lCounter,0)%></td><%		Next %></tr><tr><td align="right" class="pequeno"><strong>%:&nbsp;</strong></td><%		For Each lCounter in aWeeks
			'Division by zero check
			'Added by HE, Feb 5, 2001
			if lTotal > 0 then
				lPercent = (lCounter/lTotal)
			else
				lPercent = 0
			end if %><td align=center class="pequeno"><%=FormatPercent(lPercent)%></td><%		Next %></tr><tr><td align="right" class="pequeno"><strong>Semana:&nbsp;</strong></td><%		For lCounter = 0 to 53 %><td align=center class="pequeno"><%=lCounter + 1 %></td><%		Next %></tr></table><%		conn.close
		'set rs=nothing
		'set conn=nothing
	End Sub
	'
	' Sub GraphMonth
	'
	' Usage:
	' sViewType - The type of content to display. Values: "Visits" or "Views".
	' lYear - a four-digit year or "" for none.
	'
	Sub GraphMonth( sViewType, lYear )
		If sViewType = "Views" Then
			sSQL = "SELECT Format( Stats.Date, 'm') as [Month], Count(Stats.IP) AS Total "
			sSQL = sSQL & "FROM Stats "
			sGroupBy = "GROUP BY Format( Stats.Date, 'm') "
			sDataSource = "Stats"
		ElseIf sViewType = "Visits" Then
			sSQL = "SELECT Format( GroupIpsByDate.Date, 'm') as [Month], Count(GroupIpsByDate.IP) AS Total "
			sSQL = sSQL & "FROM GroupIpsByDate "
			sGroupBy = "GROUP BY Format( GroupIpsByDate.Date, 'm') "
			sDataSource = "GroupIpsByDate"
		Else
			Exit Sub
		End If
		sHaving = sHaving & "HAVING ((1=1) "
		If Len( lYear ) > 0 Then
			sHaving = sHaving & "AND (Format( "& sDataSource &".Date, 'yyyy')=" & lYear & ") "
			sGroupBy = sGroupBy & ", Format( "& sDataSource &".Date, 'yyyy') "
		End If
		sHaving = sHaving & ")"
		sSQL = sSQL & sGroupBy & sHaving
		' Connect to the database
		OpenDB sConnStats
		rs.Open sSQL,,,adCmdTable
		Dim aMonths(11)
		lTotal = 0
		lTop = 0
		' Calculate totals
		do while not rs.eof
			If rs("Total") > lTop Then
				lTop = rs("Total")
			End If
			lTotal = lTotal + rs("Total")
			aMonths( rs("Month") -1) = rs("Total")
		rs.movenext
		loop
		' Display results %><br><strong class="grande">&raquo; Estat&iacute;stica: <%=sViewType%> por m&ecirc;s</strong><br><br><table border="0" cellpadding="0" cellspacing="5" class="caixabranca"><tr><td valign=bottom class="pequeno"></td><% 	For Each lCounter in aMonths
			'Division by zero check
			'Added by HE, Feb 5, 2001
			if lTop > 0 then
				lHeight = (lCounter/lTop ) * 150
			else
				lHeight = 0
			end if %><td height=150 align=center valign=bottom class="pequeno"><img src="dr.gif" height=<%=lHeight%> width=10 alt="<%=lCounter%>"></td><%		Next %></tr><tr><td align="right" class="pequeno"><strong><%=sViewType%>:&nbsp;</strong></td><%		For Each lCounter in aMonths %><td align=center class="pequeno"><%=FormatNumber(lCounter,0)%></td><%		Next %></tr><tr><td align="right" class="pequeno"><strong>%:&nbsp;</strong></td><%		For Each lCounter in aMonths %><td align=center class="pequeno"><%=FormatPercent(lCounter/lTotal)%></td><%		Next %></tr><tr><td align="right" class="pequeno"><strong>M&ecirc;s:&nbsp;</strong></td><%		For lCounter = 0 to 11
			sMonth = ""
			If lYear > 0 Then
				sMonth = "<a href=""reportpathd.asp?year=" & lYear & "&month=" & lCounter + 1 & """>" & MonthName( lCounter + 1 ) & "</a>"
			Else
				sMonth = MonthName( lCounter + 1 )
			End If %><td align=center class="pequeno"><%=sMonth%></td><%		Next %></tr></table><%		conn.close
		'set rs=nothing
		'set conn=nothing
	End Sub
	'
	' Sub GraphYear
	'
	' Usage:
	' sViewType - The type of content to display. Values: "Visits" or "Views".
	'
	Sub GraphYear( sViewType )
		If sViewType = "Views" Then
			sSQL = "SELECT Format( Stats.Date, 'yyyy') as [Year], Count(Stats.IP) AS Total "
			sSQL = sSQL & "FROM Stats "
			sSQL = sSql &"GROUP BY Format( Stats.Date, 'yyyy') "
		ElseIf sViewType = "Visits" Then
			sSQL = "SELECT Format( GroupIpsByDate.Date, 'yyyy') as [Year], Count(GroupIpsByDate.IP) AS Total "
			sSQL = sSQL & "FROM GroupIpsByDate "
			sSQL = sSQL & "GROUP BY Format( GroupIpsByDate.Date, 'yyyy') "
		Else
			Exit Sub
		End If
		' Connect to the database
		OpenDB sConnStats
		rs.Open sSQL,,,adCmdTable
		Dim aYears( 25 ) ' BAD BAD BAD. I don't like it being a fixed size, but it seemed like the most efficient.
		Dim aYearValues( 25 ) ' BAD BAD BAD. I don't like it being a fixed size, but it seemed like the most efficient.
		lTotal = 0
		lTop = 0
		lCounter = 0
		' Calculate totals
		do while not rs.eof
			If rs("Total") > lTop Then
				lTop = rs("Total")
			End If
			lTotal = lTotal + rs("Total")
			aYearValues( lCounter) = rs("Total")
			aYears( lCounter ) = rs("Year")
			lCounter = lCounter + 1 
		rs.movenext
		loop	
		' Display results %><br><strong class="grande">&raquo; Estat&iacute;stica: <%=sViewType%> por ano</strong><br><br><table border="0" cellpadding="0" cellspacing="5" class="caixabranca"><tr class="pequeno"><td valign=bottom></td><%		For Each lCounter in aYearValues
			If lCounter = 0 Then
				Exit For
			End If
			'Division by zero check
			'Added by HE, Feb 5, 2001
			if lTop > 0 then
				lHeight = (lCounter/lTop ) * 150
			else
				lHeight = 0
			end if %><td height=150 align=center valign=bottom><img src="dr.gif" height=<%=lHeight%> width=10 alt="<%=lCounter%>"></td><% 	Next %></tr><tr class="pequeno"><td align="right"><strong><%=sViewType%>:&nbsp;</strong></td><% 	For Each lCounter in aYearValues
			If lCounter = 0 Then
				Exit For
			End If %><td align=center><%=FormatNumber(lCounter,0)%></td><% 	Next %></tr><tr class="pequeno"><td align="right"><strong>%:&nbsp;</strong></td><%		For Each lCounter in aYearValues
			If lCounter = 0 Then
				Exit For
			End If
			if lTotal > 0 then
				lPercent = lCounter / lTotal
			else
				lPercent = 0
			end if %><td align=center><%=FormatPercent(lPercent)%></td><%		Next %></tr><tr class="pequeno"><td align="right"><strong>Ano:&nbsp;</strong></td><%		For Each lCounter in aYears
			If lCounter = 0 Then
				Exit For
			End If %><td align=center><%=lCounter %></td><% 	Next %></tr></table><%		conn.close
		'set rs=nothing
		'set conn=nothing
	End Sub
	Function GetDate
		If Len(Request.QueryString("Month")) > 0 Then 
			GetDate = MonthName(Request.QueryString("Month"))
		If Len(Request.QueryString("Day")) > 0 Then 
			GetDate = GetDate & " " & Request.QueryString("Day")
		End If
			GetDate = GetDate & ", "
		End If
		If Len(Request.QueryString("Year")) > 0 Then
			GetDate = GetDate & Request.QueryString("Year")
		End If
		If Len(GetDate) = 0 Then
			GetDate = "All Data"
		End If
	End Function %><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5" class="caixacinza"><tr><td class="pequeno"><%	sBackCode = "<br><br><b>Visualizando dados para " & GetDate & "</b><br>"
   sType = Request.QueryString( "Type" )
   Response.Write( "� <a href=""reports.asp"">Relat�rios</a> � Gr�ficos � ")
   Select Case sType
      Case "Hour"
         Response.Write( "Relat�rio Hora" & sBackCode )
         Call GraphHours ( "Views", Request.QueryString("Year"), Request.QueryString("Month"), Request.QueryString("Day") )
         Call GraphHours ( "Visitas", Request.QueryString("Year"), Request.QueryString("Month"), Request.QueryString("Day") )
      Case "DOW"
         Response.Write( "Relat�rio Di�rio/Semana" & sBackCode )
         Call GraphDayOfWeek ( "Views", Request.QueryString("Year"), Request.QueryString("Month"), Request.QueryString("Week") )
         Call GraphDayOfWeek ( "Visitas", Request.QueryString("Year"), Request.QueryString("Month"), Request.QueryString("Week") )
      Case "DOM"
         Response.Write( "Relat�rio Di�rio/M�s" & sBackCode )
         Call GraphDayOfMonth ( "Views", Request.QueryString("Year"), Request.QueryString("Month"), Request.QueryString("Week") )
         Call GraphDayOfMonth ( "Visitas", Request.QueryString("Year"), Request.QueryString("Month"), Request.QueryString("Week") )
      Case "Week"
         Response.Write( "Relat�rio Semanal/Ano" & sBackCode )
         Call GraphWeek ( "Views", Request.QueryString("Year") )
         Call GraphWeek ( "Visitas", Request.QueryString("Year") )
      Case "Month"
         Response.Write( "Relat�rio Mensal" & sBackCode )
         Call GraphMonth ( "Views", Request.QueryString("Year") )
         Call GraphMonth ( "Visitas", Request.QueryString("Year") )
      Case "Year"
         Response.Write( "Relat�rio Mensal" & sBackCode )
         Call GraphYear ( "Views" )
         Call GraphYear ( "Visitas")
      End Select %></td></tr></table><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr align="center"><td colspan="2" class="pequeno"><table border="0" cellspacing="0" cellpadding="0"><tr><td><input name="back" type="submit" class="textoboxcinza" id="back" onClick="MM_goToURL('parent','reports.asp');return document.MM_returnValue" value="Voltar para Pagina Principal"></td><td width="5">&nbsp;</td><td><input name="back2" type="submit" class="textoboxcinza" id="back22" onClick="MM_goToURL('parent','admin.asp');return document.MM_returnValue" value="P&aacute;gina de Administra&ccedil;&atilde;o"></td><td width="5">&nbsp;</td><td><input name="back2" type="submit" class="textoboxcinza" id="back" onClick="MM_goToURL('parent','default.asp?acao=logoff');return document.MM_returnValue" value="   Efetuar Logoff...   "></td></tr></table></td></tr><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr><td class="pequeno">Visite <a href="../redirect.asp?http://www.2enetworx.com/dev">2eNetWorX</a> para saber mais sobre OpenSource VB e ASP Projects. Traduzido por <a href="http://www.telosonline.rg.com.br/">Telos Online</a>.</td><td align="right"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/statcountex.asp"><img src="icon.statcountex.gif" width=80 height=15 alt="StatCounteX" border="0"></a></td></tr></table></body></html>