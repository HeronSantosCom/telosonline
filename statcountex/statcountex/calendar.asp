<% 
'************************************************
'***** Written By Don Lasseigne *****************
'***** dlasseigne@yahoo.com *********************
'************************************************
'* Here is Calendar Program I wrote for REPORTS.ASP
'* Just put an Include Statement in Reports.ASP
'* 
'* Feel free to do with as you like
'************************************************
'*BEGIN MAIN ************************************

  mMonth = request.querystring("month")
  yYear =  request.querystring("year")
  if mMonth = "" then mMonth= Month(now)
  if yYear  = "" then yYear = Year(now)

  Call DisplayCalendar(mMonth, yYear)

'*END MAIN **************************************
'************************************************
Sub DisplayCalendar(mMonth, yYear)
  
  Title = MonthName(mMonth) & " " & yYear
  dDay = DateSerial(yYear,mMonth,1)
  mMonth = Month(dDay)
  yYear = Year(dDay)
  FirstDayOfMonth =  Weekday(dDay)

 'Response.Write "<B class="grande">" & Title & </B><br>" & vbCrlf
 Response.Write "<table width=300 border=0 align='center' cellpadding='0' cellspacing=5 class='caixabranca'>" & vbCrlf

  Response.Write "<tr valign='Top' align='center' class='pequeno'>" & vbCrlf
  For i = vbSunday To vbSaturday 
     Response.Write  "<td width='15%'><b>" & left(WeekDayName(i), 3) & "</b></td>" & vbCrlf
  Next
  Response.Write "</tr>" & vbCrlf

  dDay = DateSerial(yYear,mMonth,1)
  For j=1 to 6
    Response.Write "<tr class='pequeno'>" & vbCrlf
    For i=vbSunday to vbSaturday
      CellStr="&nbsp;" 
		CellColor="#FFFFFF"
      If WeekDay(dDay) = i and Month(dDay) = mMonth then 
        if (Day(dDay) = Day(now)) and (Month(dDay) = Month(now)) and (Year(dDay) = Year(now)) then CellColor="#EFEFEF"
        CellStr = "<a href='reportpathdd.asp?year=" & Year(dDay) & "&month=" & Month(dDay) & "&day=" & Day(dDay) & "'>" & Day(dDay) & "</a>"
        dDay = DateAdd("d",1,dDay)
      End If
      Response.Write "<td align=center bgcolor=" & CellColor & ">"  & CellStr & "</td>" & vbCrlf
    Next
   Response.Write "</tr>" & vbCrlf
  Next

  Response.Write "</table>" & vbCrlf

  Call DisplaySelectDate()
  
End Sub
'************************************************
'************************************************
Sub DisplaySelectDate()
  Response.Write "<BR><table border=0 cellpadding='0' cellspacing=5 class='caixabranca'><form method='get'><tr><td>" & vbCrlf
  Call MonthCombo()
  Call YearCombo()
  Response.Write "<input type='submit' value='  Ok  ' class='textoboxcinza'>"
  Response.Write "</td></tr></form></table>"
End Sub
'************************************************
'************************************************
Sub MonthCombo()
  Response.Write "<select name='Month' class='textobox'>"
  Response.Write "<Option value=" & mMonth & ">" & monthName(mMonth) & "</Option>"
  Response.Write "<Option value=1>Janeiro</Option>"
  Response.Write "<Option value=2>Fevereiro</Option>"
  Response.Write "<Option value=3>Mar�o</Option>"
  Response.Write "<Option value=4>Abril</Option>"
  Response.Write "<Option value=5>Maio</Option>"
  Response.Write "<Option value=6>Junho</Option>"
  Response.Write "<Option value=7>Julho</Option>"
  Response.Write "<Option value=8>Agosto</Option>"
  Response.Write "<Option value=9>Setembro</Option>"
  Response.Write "<Option value=10>Outubro</Option>"
  Response.Write "<Option value=11>Novembro</Option>"
  Response.Write "<Option value=12>Dezembro</Option>"
  Response.Write "</select>"
End Sub
'************************************************
'************************************************
Sub YearCombo()
  Response.Write "<select name='Year' class='textobox'>"
  Response.Write "<Option value=" & yYear & ">" & yYear & "</Option>"
  For i=2001 To 2010  
    Response.Write  "<Option value=" &  i & ">" & i & "</Option>"
  Next
  Response.Write "</select>"
End Sub
'************************************************
%>