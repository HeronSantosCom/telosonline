<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%><%pageProtege = True%><!--#include file="config.asp"--><html><head><title>StatCounteX 3.2</title><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><link href="style.css" rel="stylesheet" type="text/css"><script language="JavaScript" type="text/JavaScript"><!--
	function MM_goToURL() { //v3.0
		var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
//--></script></head><body><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td class="grande"><strong>StatCounteX 3.2 - Relat&oacute;rio</strong></td><td align="right" class="pequeno"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/reportbug.asp?pid=4" target="_blank">Reportar um Erro</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/recommend.asp?pid=4" target="_blank">Recomende este projeto </a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/question.asp?pid=4" target="_blank">FAQ</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/submitsite.asp?pid=4" target="_blank">Inscrever um site</a></td></tr><tr><td colspan="2"><hr width="100%" color="#336699" size="1" noshade></td></tr></table><% '-------------------------------------------------------------
'StatCounteX 3.2
'http://www.2enetworx.com/dev/projects/statcountex.asp

'File: reportref.asp
'Description: Referers Report
'Initiated by Hakan Eskici on Nov 18, 2000
'-------------------------------------------------------------
'Credits:
'Kevin Yochum, Warp Drive Enterprises (http://www.warp-drive.com)
'Great additions to StatCounteX including but not limited to: 
'Graphical Reports, Top Day, Visitor Path Behaviour
'IP Filter, Active Links

'You may use the code for any purpose
'But re-publishing is discouraged.
'See License.txt for additional information

'-------------------------------------------------------------
'Change Log:
'-------------------------------------------------------------
'# Feb 5, 2001 by Kevin Yochum
'Added links for graphical reports and visitor ip paths
'------------------------------------------------------------- %><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5" class="caixacinza"><tr><td class="pequeno"><a href="reports.asp">Relat&oacute;rio</a> � Refer&ecirc;ncia<br><%	Sub SQLTable(sTitle, sSQL)
		rs.Open sSQL, , , adCmdTable %><br><strong class="grande">&raquo;&nbsp;<%=sTitle%><br><br></strong><table width='100%' border=0 cellpadding=0 cellspacing=5 class="caixabranca"><tr><%		for each fld in rs.fields
			iFieldCount = iFieldCount + 1
			if iFieldCount = 1 then
				sAlign = "left"
			else
				sAlign="right"
			end if %><td class="pequeno"><strong><%=fld.name%></strong></td><%		next %></tr><%		do while not rs.eof %><tr><%			iFieldCount = 0
			for each fld in rs.fields
				iFieldCount = iFieldCount + 1
				if iFieldCount = 1 then
					iWidth=400
					sAlign = "left"
				else
					iWidth=50
					sAlign="right"
				end if
				sFieldName = ""
				If Len(Trim(rs(fld.name))) > 100 Then
					fldName = Mid(rs(fld.name),1,100) & "..."
				Else
					fldName = rs(fld.name)
				End If
				If bShowLinks And InStr( rs(fld.name), "http://" ) > 0 Then
					sFieldName = "<a href='" & rs(fld.name) & "'>" & fldName & "</a>"
				Else
					sFieldName = fldName
				End if %><td class="pequeno" align="<%=sAlign%>"><%=sFieldName%></td><%			next %></tr><%		rs.movenext
		loop %></table><%		rs.close
	End Sub
	OpenDB sConnStats
	SQLTable "Referers", "SELECT RefName, Total FROM Refs ORDER BY Total DESC"
	conn.close
	set rs=nothing
	set conn=nothing %></td></tr></table><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr align="center"><td colspan="2" class="pequeno"><table border="0" cellspacing="0" cellpadding="0"><tr><td><input name="back" type="submit" class="textoboxcinza" id="back" onClick="MM_goToURL('parent','reports.asp');return document.MM_returnValue" value="Voltar para Pagina Principal"></td><td width="5">&nbsp;</td><td><input name="back2" type="submit" class="textoboxcinza" id="back22" onClick="MM_goToURL('parent','admin.asp');return document.MM_returnValue" value="P&aacute;gina de Administra&ccedil;&atilde;o"></td><td width="5">&nbsp;</td><td><input name="back2" type="submit" class="textoboxcinza" id="back" onClick="MM_goToURL('parent','default.asp?acao=logoff');return document.MM_returnValue" value="   Efetuar Logoff...   "></td></tr></table></td></tr><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr><td class="pequeno">Visite <a href="../redirect.asp?http://www.2enetworx.com/dev">2eNetWorX</a> para saber mais sobre OpenSource VB e ASP Projects. Traduzido por <a href="http://www.telosonline.rg.com.br/">Telos Online</a>.</td><td align="right"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/statcountex.asp"><img src="icon.statcountex.gif" width=80 height=15 alt="StatCounteX" border="0"></a></td></tr></table></body></html>