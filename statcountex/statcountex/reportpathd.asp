<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%><%pageProtege = True%><!--#include file="config.asp"--><html><head><title>StatCounteX 3.2</title><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><link href="style.css" rel="stylesheet" type="text/css"><script language="JavaScript" type="text/JavaScript"><!--
function MM_goToURL() { //v3.0
var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//--></script></head><body><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td class="grande"><strong>StatCounteX 3.2 - Relat&oacute;rio</strong></td><td align="right" class="pequeno"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/reportbug.asp?pid=4" target="_blank">Reportar um Erro</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/recommend.asp?pid=4" target="_blank">Recomende este projeto </a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/question.asp?pid=4" target="_blank">FAQ</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/submitsite.asp?pid=4" target="_blank">Inscrever um site</a></td></tr><tr><td colspan="2"><hr width="100%" color="#336699" size="1" noshade></td></tr></table><%
'-------------------------------------------------------------
'StatCounteX 3.2
'http://www.2enetworx.com/dev/projects/statcountex.asp

'File: reportpathd.asp
'Description: Daily Report
'Initiated by Hakan Eskici on Nov 18, 2000

'See credits.txt for the list of contributors

'You may use the code for any purpose
'But re-publishing is discouraged.
'See License.txt for additional information

'-------------------------------------------------------------
'Change Log:
'-------------------------------------------------------------
'# Feb 5, 2001 by Kevin Yochum
'Added links for graphical reports and visitor ip paths
'Added showing url's as active links
'-------------------------------------------------------------
%><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5" class="caixacinza"><tr><td class="pequeno"><a href="reports.asp">Relat&oacute;rio</a> � <a href="reportpathy.asp">Relat&oacute;rio Anual </a> � <a href="reportpathm.asp?year=<%=request("year")%>">Relat&oacute;rio Mensal</a> � Relat&oacute;rio Di&aacute;rio<br><br><strong class="grande">&raquo; Page Views & Visitantes &uacute;nico por <%=MonthName(request("month")) & " " & request("year") %></strong><br><br><table width="25%" border=0 cellpadding=0 cellspacing=5 class="caixabranca"><tr align="center" class="pequeno"><td><strong>Dia</strong></td><td><strong>Views</strong></td><td><strong>Visitantes</strong></td></tr><%	sYear = request("year")
	sMonth = request("month")
	OpenDB sConnStats
	sSQL = "SELECT Sum(TopIpsPerDay.Total) AS Ips, Sum(TopPageViewsPerDay.Total) AS Views, Format([TopIpsPerDay].[Date],'d') AS DayNumber FROM TopIpsPerDay INNER JOIN TopPageViewsPerDay ON TopIpsPerDay.Date = TopPageViewsPerDay.Date GROUP BY Format([TopIpsPerDay].[Date],'d'), Format([topIpsPerDay].[Date],'yyyy'), Format([topIpsPerDay].[Date],'m') HAVING (((Format([topIpsPerDay].[Date],'yyyy'))="&sYear&") AND ((Format([topIpsPerDay].[Date],'m'))="&sMonth&"))"
	rs.Open sSQL,,,adCmdTable
	do while not rs.eof %><tr align="center" class="pequeno"><td><a href="reportpathdd.asp?year=<%=sYear%>&month=<%=sMonth%>&day=<%=rs("DayNumber")%>"><%=rs("DayNumber")%></a></td><td><%=rs("Views")%></td><td><%=rs("Ips")%></td></tr><%	rs.movenext
	loop
	conn.close
	set rs=nothing
	set conn=nothing %></table><br><strong class="grande">Estat&iacute;stica Detalhada (<%=MonthName(sMonth) & " " & sYear%>)</strong><br>  
�  Ver <a href="ips.asp?Year=<%=sYear%>&Month=<%=sMonth%>">clique paths (Endere&ccedil;o IP)</a>.<br><br><strong class="grande">Gr&aacute;ficos (<%=MonthName(sMonth) & " " & sYear%>)</strong><br>  
  �  Ver <a href="graphs.asp?Type=Hour&Year=<%=sYear%>&Month=<%=sMonth%>">Page Views e Visitantes por Hora</a>.<br>
  � 
  Ver <a href="graphs.asp?Type=DOW&Year=<%=sYear%>&Month=<%=sMonth%>">Page Views e Visitantes por Dia/Semana</a>.<br>
  � Ver <a href="graphs.asp?Type=DOM&Year=<%=sYear%>&Month=<%=sMonth%>">Page Views e Visitantes por Dia/M&ecirc;s</a>.</td></tr></table><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr align="center"><td colspan="2" class="pequeno"><table border="0" cellspacing="0" cellpadding="0"><tr><td><input name="back" type="submit" class="textoboxcinza" id="back" onClick="MM_goToURL('parent','reports.asp');return document.MM_returnValue" value="Voltar para Pagina Principal"></td><td width="5">&nbsp;</td><td><input name="back2" type="submit" class="textoboxcinza" id="back22" onClick="MM_goToURL('parent','admin.asp');return document.MM_returnValue" value="P&aacute;gina de Administra&ccedil;&atilde;o"></td><td width="5">&nbsp;</td><td><input name="back2" type="submit" class="textoboxcinza" id="back" onClick="MM_goToURL('parent','default.asp?acao=logoff');return document.MM_returnValue" value="   Efetuar Logoff...   "></td></tr></table></td></tr><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr><td class="pequeno">Visite <a href="../redirect.asp?http://www.2enetworx.com/dev">2eNetWorX</a> para saber mais sobre OpenSource VB e ASP Projects. Traduzido por <a href="http://www.telosonline.rg.com.br/">Telos Online</a>.</td><td align="right"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/statcountex.asp"><img src="icon.statcountex.gif" width=80 height=15 alt="StatCounteX" border="0"></a></td></tr></table></body></html>