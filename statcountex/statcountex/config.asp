<%
	'-------------------------------------------------------------
	'StatCounteX 3.2
	'http://www.2enetworx.com/dev/projects/statcountex.asp
	
	'File: admin.asp
	'Description: StatCounteX Reports Main Page
	'Initiated by Hakan Eskici on Nov 18, 2000

	'See credits.txt for the list of contributors
	
	'You may use the code for any purpose
	'But re-publishing is discouraged.
	'See License.txt for additional information	

	'-------------------------------------------------------------
	'Change Log:
	'-------------------------------------------------------------
	'# Feb 25, 2001 by Rami Kattan
	'WindowsME can be counted now (database file need a new entry in OSes: OsName= WinMe, OsId = 8
	'Netscape 6.x is reported as NS 6.x

	'# Feb 20, 2001 by FlipDaMusic
	'Future Support for SQL6, SQL7, Access 2000, Access97 (not fully implemented)
	'Add Loading of Config Variables

	'# Feb 5, 2001 by Kevin Yochum
	'Added IP filter to ignore hits from specified visitor IP's
	'Added configuration switch to display url's as active links
	'-------------------------------------------------------------

	'-------------------------------------------------
	'You don't need to modify anything below this line.
	'-------------------------------------------------
	
	'Construct the connection string
	
	'## Make sure to uncomment one of the sConnStats lines!
	'## MS Access 97
	'sConnStats = "DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=c:\inetpub\wwwroot\scx\stats.mdb" 
	
	'## MS Access 2000 using virtual path
	sConnStats = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Server.MapPath("stats.mdb") 
	
	'## MS Access 2000
	'sConnStats = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=c:\inetpub\wwwroot\scx\stats.mdb;" 

	'## MS SQL Server 7
	'sConnStats = "driver={SQL Server};server=SERVERNAME;uid=USERNAME;pwd=PASSWORD;database=DATABASENAME" 
	
	
	'Pre Create the connection and recordset objects
	set conn = Server.CreateObject("ADODB.Connection")
	set rs = Server.CreateObject("ADODB.Recordset")

	'ADO Constants
	'---- CursorTypeEnum Values ----
	Const adOpenForwardOnly = 0
	Const adOpenKeyset = 1
	Const adOpenDynamic = 2
	Const adOpenStatic = 3

	'---- CursorLocationEnum Values ----
	Const adUseServer = 2
	Const adUseClient = 3

	'---- CommandTypeEnum Values ----
	Const adCmdUnknown = &H0008
	Const adCmdText = &H0001
	Const adCmdTable = &H0002
	Const adCmdStoredProc = &H0004
	Const adCmdFile = &H0100
	Const adCmdTableDirect = &H0200
	
	sub OpenDB(sConn)
		'Opens the given connection and initializes the recordset
		conn.open sConn
		set rs.ActiveConnection = conn
		rs.CursorType = adOpenStatic
	end sub
	
	sub CloseDB()
		'Closes and destroys the connection and recordset objects
		rs.close
		conn.close
		set rs = nothing
		set conn = nothing
	end sub
	
	sub w(sText)
		'A Quickie ;)
		response.write sText & vbCrLf
	end sub

	'Load Config from DB
	
	'Open Connection
	conn.open sConnStats
		
	'Build SqlString
	strSql = "SELECT C_ImageLoc "
	strSql = strSql & ", C_FilterIP "
	strSql = strSql & ", C_ShowLinks "
	strSql = strSql & ", C_RefThisServer "
	strSql = strSql & ", C_StripPathParameters "
	strSql = strSql & ", C_StripPathProtocol "
	strSql = strSql & ", C_StripRefParameters "
	strSql = strSql & ", C_StripRefProtocol "
	strSql = strSql & ", C_StripRefFile "
	strSql = strSql & "FROM Config WHERE ID = 1"
				
	'Open RecordSet
	set rs = conn.Execute(strSql)
		
	'Get Variables
	sImageLocation			= rs.Fields("C_ImageLoc")
	sFilterIps				= rs.Fields("C_FilterIP")
	bShowLinks				= rs.Fields("C_ShowLinks")
	bRefThisServer			= rs.Fields("C_RefThisServer")
	bStripPathParameters	= rs.Fields("C_StripPathParameters")
	bStripPathProtocol		= rs.Fields("C_StripPathProtocol")
	bStripRefParameters 	= rs.Fields("C_StripRefParameters")
	bStripRefProtocol		= rs.Fields("C_StripRefProtocol")
	bStripRefFile			= rs.Fields("C_StripRefFile")
	
	'Terminate database connection
	rs.Close
	conn.close

	' EMAIL DO ADMINISTRADOR
	adminEmail = "admin@admin.com"
	adminSenha = "admin"
	
	' MANIPULA COOKIE[EMAIL]
	If Len(Trim(logEmail)) = 0 Then
		logEmail = Trim(Request.Cookies("telosonline")("email"))
	End IF
	
	If Len(Trim(logEmail)) <> 0 Then
		logLogado = True
	End If
	
	' P�GINA RESTRITA
	If pageProtege and Trim(logEmail) <> adminEmail Then
		Response.Redirect("default.asp")
	End If %>