<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%><%pageProtege = True%><!--#include file="config.asp"--><html><head><title>StatCounteX 3.2</title><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><link href="style.css" rel="stylesheet" type="text/css"><script language="JavaScript" type="text/JavaScript"><!--
	function MM_goToURL() { //v3.0
		var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
		for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
	}
//--></script></head><body><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td class="grande"><strong>StatCounteX 3.2 - Relat&oacute;rio</strong></td><td align="right" class="pequeno"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/reportbug.asp?pid=4" target="_blank">Reportar um Erro</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/recommend.asp?pid=4" target="_blank">Recomende este projeto </a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/question.asp?pid=4" target="_blank">FAQ</a> | <a href="../redirect.asp?http://www.2enetworx.com/dev/projects/submitsite.asp?pid=4" target="_blank">Inscrever um site</a></td></tr><tr><td colspan="2"><hr width="100%" color="#336699" size="1" noshade></td></tr></table><%'-------------------------------------------------------------
	'StatCounteX 3.2
	'http://www.2enetworx.com/dev/projects/statcountex.asp
	
	'File: config.asp
	'Description: Graphical Report Generator
	'Initiated by Kevin Yochum on Feb 5, 2001
	'-------------------------------------------------------------
	
	'You may use the code for any purpose
	'But re-publishing is discouraged.
	'See License.txt for additional information
	
	'-------------------------------------------------------------
	'Change Log:
	'-------------------------------------------------------------
	'# Feb 12, 2001 by Hakan Eskici
	'Added missing </table> tags
	'-------------------------------------------------------------
	
	' Sub ListIps
	'
	' Usage:
	' lYear - the numerical year (optional)
	' lMonth - the numerical month (optional)
	' lDay - the numerical day (optional)
	' lHour - the numerical hour (optional)
	'
	Sub ListIps( lYear, lMonth, lDay, lHour ) 
		sDataSource = ""
		If Len(lHour) > 0 Then
			sDataSource = "GroupIpsByHourAndDate"
		Else
			sDataSource = "GroupIpsByDate"
		End If
		sSQL = "SELECT * From " & sDataSource & " Where 1=1 "
		If Len( lYear ) > 0 Then
			sSQL = sSQL & "and DatePart(""yyyy"",[Date])=" & lYear & " "
		End If
		If Len( lMonth ) > 0 Then
			sSQL = sSQL & "and DatePart( ""m"", [Date])= " & lMonth & " "
		End If
		If Len( lDay ) > 0 Then
			sSQL = sSQL & "and DatePart( ""d"", [Date])= " & lDay & " "
		End If
		If Len( lHour ) > 0 Then
			sSQL = sSQL & "and Hour= " & lHour & " "
		End If
		' Connect to the database
		OpenDB sConnStats
		rs.Open sSQL,,,adCmdTable %><strong class="grande">&raquo; Estat&iacute;stica: Visitantes em <%=GetDate%></strong><br><br><table width="25%" border="0" cellpadding="0" cellspacing="5" class="caixabranca"><%		do while not rs.eof
			sLink = "<a href=""ips.asp?ip=" & rs("IP")
			If Len( lYear ) > 0 Then
				sLink = sLink & "&year=" & lYear
			End If
			If Len( lMonth ) > 0 Then
				sLink = sLink & "&month=" & lMonth
			End If
			If Len( lDay ) > 0 Then
				sLink = sLink & "&day=" & lDay
			End If
			If Len( lHour ) > 0 Then
				sLink = sLink & "&hour=" & lHour
			End If
				sLink = sLink & """>" & rs("IP") & "</a>" %><tr><td align="center" class="pequeno"><%=sLink%></td></tr><%		rs.movenext
		loop %><tr><td align="center" class="pequeno"><strong><%=rs.RecordCount%>  visitas</strong></td></tr></table><%		conn.close
		'set rs=nothing
		'set conn=nothing
	End Sub
	'
	' Sub ShowClickPath
	'
	' Usage:
	' sIP - The IP to show the click path for.
	' lYear - the numerical year (optional)
	' lMonth - the numerical month (optional)
	' lDay - the numerical day (optional)
	' lHour - the numerical hour (optional)
	'
	Sub ShowClickPath( sIp, lYear, lMonth, lDay, lHour ) 
		If Len( sIP ) = 0 Then
			Response.Write( "Error: IP Address not provided for displaying a Click Path." )
			Exit Sub
		End If
		' sSQL = "SELECT Stats.*, Refs.RefName, Paths.PathName FROM Paths RIGHT JOIN (Refs RIGHT JOIN Stats ON Refs.RefID = Stats.RefID) ON Paths.PathID = Stats.PathID WHERE (((Stats.IP)='"&sIp&"') AND ((Stats.Date)="&sDate&"))"
		sSQL = "SELECT Stats.Date, Stats.Time, Stats.IP, Paths.PathName, Refs.RefName FROM Paths RIGHT JOIN (Refs RIGHT JOIN Stats ON Refs.RefID = Stats.RefID) ON Paths.PathID = Stats.PathID Where Stats.IP='" & sIp & "'"
		If Len( lYear ) > 0 Then
			sSQL = sSQL & " and DatePart(""yyyy"", [Stats].[Date]) = " & lYear
		End If
		If Len( lMonth ) > 0 Then
			sSQL = sSQL & " and DatePart(""m"", [Stats].[Date]) = " & lMonth
		End If
		If Len( lDay ) > 0 Then
			sSQL = sSQL & " and DatePart(""d"", [Stats].[Date]) = " & lDay
		End If
		If Len( lHour ) > 0 Then
			sSQL = sSQL & " and DatePart(""h"", [Stats].[Time]) = " & lHour
		End If
		' Connect to the database
		OpenDB sConnStats
		rs.Open sSQL,,,adCmdTable
		sFieldName = ""
		If bShowLinks And InStr( rs("RefName"), "http://" ) > 0 Then
			sFieldName = "<a href=""" & rs("RefName") & """>" & rs("RefName") & "</a>"
		Else
			sFieldName = rs("RefName")
		end if %><strong class="grande">&raquo; Estat&iacute;stica: Cliques por <%=sIp%> em <%=rs("Date")%></strong><br><br><table width="25%" border=0 cellpadding=0 cellspacing=5 class="caixabranca"><tr align="center" class="pequeno"><td><strong>Dia</strong></td><td><strong>Hor&aacute;rio</strong></td><td><strong>P&aacute;gina</strong></td></tr><tr class="pequeno"><td><%=rs("Date")%></td><td><%=rs("Time")%></td><td><%=sFieldName%></td></tr><%		' Calculate totals
		do while not rs.eof
			sFieldName = ""
			If bShowLinks And InStr( rs("PathName"), "http://" ) > 0 Then
				sFieldName = "<a href=""" & rs("PathName") & """>" & rs("PathName") & "</a>"
			Else
				sFieldName = rs("RefName")
			end if %><tr class="pequeno"><td><%=rs("Date")%></td><td><%=rs("Time")%></td><td><%=sFieldName%></td></tr><%		rs.movenext
		loop %></table><%		conn.close
		'set rs=nothing
		'set conn=nothing
	End Sub
	Function GetDate
		If Len(Request.QueryString("Month")) > 0 Then 
			GetDate = MonthName(Request.QueryString("Month"))
			If Len(Request.QueryString("Day")) > 0 Then 
				GetDate = GetDate & " " & Request.QueryString("Day")
			End If
			GetDate = GetDate & ", "
		End If
		If Len(Request.QueryString("Year")) > 0 Then
			GetDate = GetDate & Request.QueryString("Year")
		End If
		If Len(GetDate) = 0 Then
			GetDate = "All Data"
		End If
	End Function
	sIp = Request.QueryString( "IP" )
	sYear = Request.QueryString( "Year" )
	sMonth = Request.QueryString( "Month" )
	sDay = Request.QueryString( "Day" )
	sHour = Request.QueryString( "Hour" ) %><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5" class="caixacinza"><tr><td class="pequeno"><a href="reports.asp">Relat&oacute;rio</a><% If Len( sIp ) > 0 Then %><a href="ips.asp?year=<%=sYear%>&month=<%=sMonth%>&day=<%=sDay%>">Relat&oacute;rio de Visitantes</a> � Cliques P&aacute;ginas
<% Else %> 
� Relat&oacute;rio de Visitantes
<% End If %><br><br><% Response.Write( "<b>Visualizando dados por " & GetDate & "</b><br><br>" )
	If Len( sIp ) > 0 Then
		ShowClickPath sIp, sYear, sMonth, sDay, sHour
	Else
		ListIps sYear, sMonth, sDay, sHour
	End If %></td></tr></table><table width="100%" border="0" align="center" cellpadding="0" cellspacing="5"><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr align="center"><td colspan="2" class="pequeno"><table border="0" cellspacing="0" cellpadding="0"><tr><td><input name="back" type="submit" class="textoboxcinza" id="back" onClick="MM_goToURL('parent','reports.asp');return document.MM_returnValue" value="Voltar para Pagina Principal"></td><td width="5">&nbsp;</td><td><input name="back2" type="submit" class="textoboxcinza" id="back22" onClick="MM_goToURL('parent','admin.asp');return document.MM_returnValue" value="P&aacute;gina de Administra&ccedil;&atilde;o"></td><td width="5">&nbsp;</td><td><input name="back2" type="submit" class="textoboxcinza" id="back" onClick="MM_goToURL('parent','default.asp?acao=logoff');return document.MM_returnValue" value="   Efetuar Logoff...   "></td></tr></table></td></tr><tr><td colspan="2" align="right"><hr width="100%" color="#336699" size="1" noshade></td></tr><tr><td class="pequeno">Visite <a href="../redirect.asp?http://www.2enetworx.com/dev">2eNetWorX</a> para saber mais sobre OpenSource VB e ASP Projects. Traduzido por <a href="http://www.telosonline.rg.com.br/">Telos Online</a>.</td><td align="right"><a href="../redirect.asp?http://www.2enetworx.com/dev/projects/statcountex.asp"><img src="icon.statcountex.gif" width=80 height=15 alt="StatCounteX" border="0"></a></td></tr></table></body></html>